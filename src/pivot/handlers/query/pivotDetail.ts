import { IPivotRepository } from "../../domain/repositories/pivot";
import { PivotDetail, PivotDetailResponse } from "../../messages/query/pivotDetail";
import { DataSourceInfo } from "../../messages/common/dataSourceInfo";

export function PivotDetailHandlerFactory(repo: IPivotRepository) {
    return async (req: PivotDetail): Promise<PivotDetailResponse | null> => {
        const widget = await repo.findById(req.id);
        if (widget == null) return null;
        return {
            id: widget._id,
            createdBy: widget.createdBy,
            createdOn: widget.createdOn,
            data: widget.data.map(p => {
                const itm: DataSourceInfo = { id: p.id, query: p.query };
                return itm;
            }),
            options: widget.options,                        
            title: widget.title            
        };
    }
}
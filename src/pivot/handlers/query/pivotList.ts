import { IPivotRepository } from "../../domain/repositories/pivot";
import { HttpContextFactory, IAppContext, AppContextTypes } from "../../../core/abstractions/appcontext";
import { PivotList, PivotListResponse } from "../../messages/query/pivotList";
import { BasicPivotInfo } from "../../messages/common/basicPivotInfo";

export function PivotListHandlerFactory(
    repo: IPivotRepository,
    contextFactory: HttpContextFactory) {
    return async (req: PivotList): Promise<PivotListResponse> => {
        const ctx = contextFactory().Get<IAppContext>(AppContextTypes.Context);
        const pivots = await repo.findByCustomerId(ctx.customerid);
        let pinfos = pivots.map(p => {
            const itm: BasicPivotInfo = {
                id: p._id,
                createdBy: p.createdBy,
                createdOn: p.createdOn,                
                title: p.title                
            };
            return itm;
        });        
        return { pivots: pinfos };
    }
}
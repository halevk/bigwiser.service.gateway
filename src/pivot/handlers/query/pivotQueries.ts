import { IPivotRepository } from "../../domain/repositories/pivot";
import { PivotQuery } from "../../messages/common/pivotQuery";
import { PivotQueriesResponse, PivotQueries } from "../../messages/query/pivotQueries";
import { DataSourceInfo } from "../../messages/common/dataSourceInfo";

export function PivotQueriesHandlerFactory(repo: IPivotRepository) {
    return async (req: PivotQueries): Promise<PivotQueriesResponse> => {
        const widgets = await repo.findByIds(req.ids);
        if (widgets.length == 0) return { queries: [] };
        const queries = widgets.map(p => {
            const itm: PivotQuery = {
                id: p._id,
                datasource: p.data.map(o => {
                    const it: DataSourceInfo = { id: o.id, query: o.query };
                    return it;
                })
            };
            return itm;
        });
        return { queries: queries };
    }
}
import { RemovePivot, RemovePivotResponse } from "../../messages/command/removePivot";
import { IPivotRepository } from "../../domain/repositories/pivot";
import { HttpContextFactory, IAppContext, AppContextTypes } from "../../../core/abstractions/appcontext";
import { IsUserMemberOf } from "../../../core/extensions/role";
import { Role } from "../../../core/abstractions/access";

export function RemovePivotHandlerFactory(
    repo: IPivotRepository,
    contextFactory: HttpContextFactory) {
    return async (req: RemovePivot): Promise<RemovePivotResponse> => {
        const ctx = contextFactory().Get<IAppContext>(AppContextTypes.Context);
        const pivot = await repo.findById(req.id);
        if (pivot == null) return { success: false, msg: "pivot not found" };
        if (IsUserMemberOf(ctx.roles, [Role.Designer]) && ctx.userId != pivot.creatorid)
            return { success: false, msg: "you can not remove this pivot" };
        const result = await repo.remove(req.id);
        return { success: result, msg: "" };
    }
}
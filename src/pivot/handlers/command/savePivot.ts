import { IPivotRepository } from "../../domain/repositories/pivot";
import { SavePivot, SavePivotResponse } from "../../messages/command/savePivot";
import { IsNullOrEmptyOrWhitespace } from "../../../core/extensions/stringextensions";
import { Pivot, DataSource } from "../../domain/models/pivot";
import { HttpContextFactory, IAppContext, AppContextTypes } from "../../../core/abstractions/appcontext";
import { GetUtcNowAsString } from "../../../core/extensions/date";
const uuidv4 = require("uuid/v4");

export function SavePivotHandlerFactory(
    repo: IPivotRepository, contextFactory: HttpContextFactory) {
    return async (req: SavePivot): Promise<SavePivotResponse> => {
        const ctx = contextFactory().Get<IAppContext>(AppContextTypes.Context);
        const isIdMissing = IsNullOrEmptyOrWhitespace(req.id)
        const id = isIdMissing ? uuidv4() : req.id;
        const user = `${ctx.name} ${ctx.surname}`;
        let pivot: Pivot | null = null;
        let datasources: DataSource[] = [];
        if (req.data.length > 0)
            datasources = req.data.map(p => {
                const itm: DataSource = { id: p.id, query: p.query };
                return itm;
            });
        if (!isIdMissing) {
            pivot = await repo.findById(id);
            if (pivot == null) return { success: false, msg: "pivot not found" };
            pivot.updatedOn = GetUtcNowAsString();
            pivot.updatedBy = user;
            pivot.options = req.options;            
            pivot.title = req.title;         
            pivot.data = datasources;
            pivot.customerid=ctx.customerid;
        }
        else {
            pivot = {
                _id: id,
                data: datasources,
                createdBy: user,
                createdOn: GetUtcNowAsString(),
                creatorid: ctx.userId,
                options: req.options,               
                title: req.title, 
                customerid:ctx.customerid
            };
        }
        const result = await repo.save(pivot);
        if (!result) return { success: false, msg: "pivot could not saved" };
        return { success: true, msg: "", id: id };
    }
}
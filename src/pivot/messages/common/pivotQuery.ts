import { DataSourceInfo } from "./dataSourceInfo";

export interface PivotQuery {
    id:string,
   datasource:DataSourceInfo[]
}
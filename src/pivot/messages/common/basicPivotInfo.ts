export interface BasicPivotInfo {
    id:string,
    title:string,   
    createdBy:string,
    createdOn:string
}
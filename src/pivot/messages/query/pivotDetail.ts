import { DataSourceInfo } from "../common/dataSourceInfo";
import { ValidationResult } from "../../../core/abstractions/routing";
import { IsNullOrEmptyOrWhitespace } from "../../../core/extensions/stringextensions";

export class PivotDetail {
    id:string="";
}

export interface PivotDetailResponse {
    id:string,
    title:string,    
    createdBy:string,
    createdOn:string,
    data: DataSourceInfo[],    
    options: any
}

export function PivotDetailValidator(request: PivotDetail): ValidationResult {
    const err: string[] = [];
    if (IsNullOrEmptyOrWhitespace(request.id)) err.push("id is required");      
    const success = err.length == 0;
    return { errors: err, success: success };

}




import { BasicPivotInfo } from "../common/basicPivotInfo";

export class PivotList{   
}

export interface PivotListResponse {
    pivots:BasicPivotInfo[];
}
import { PivotQuery } from "../common/pivotQuery";
import { ValidationResult } from "../../../core/abstractions/routing";
import { isDate } from "util";

export class PivotQueries {
    ids: string[] = [];
}

export interface PivotQueriesResponse {
    queries: PivotQuery[];
}

export function PivotQueriesValidator(request: PivotQueries): ValidationResult {
    const err: string[] = [];
    if (isDate.length==0) err.push("ids are required");      
    const success = err.length == 0;
    return { errors: err, success: success };
}
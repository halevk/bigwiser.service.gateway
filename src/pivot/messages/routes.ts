export const SavePivotRoute = "/pivot/save";
export const PivotListRoute = "/pivot/list";
export const PivotDetailRoute = "/pivot/detail";
export const PivotQueriesRoute="/pivot/query";
export const RemovePivotRoute="/pivot/remove";
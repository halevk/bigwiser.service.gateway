import { ValidationResult } from "../../../core/abstractions/routing";
import { IsNullOrEmptyOrWhitespace } from "../../../core/extensions/stringextensions";

export class RemovePivot {
    id: string = "";    
}

export interface RemovePivotResponse {
    success: boolean,
    msg: string    
}

export function RemovePivotValidator(request: RemovePivot): ValidationResult {
    const err: string[] = [];
    if (IsNullOrEmptyOrWhitespace(request.id)) err.push("title is required");      
    const success = err.length == 0;
    return { errors: err, success: success };
}
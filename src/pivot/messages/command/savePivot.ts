import { DataSourceInfo } from "../common/dataSourceInfo";
import { ValidationResult } from "../../../core/abstractions/routing";
import { IsNullOrEmptyOrWhitespace } from "../../../core/extensions/stringextensions";

export class SavePivot {
    id: string = "";
    title: string = "";    
    data: DataSourceInfo[] = [];   
    options: any = null;
}

export interface SavePivotResponse {
    success: boolean,
    msg: string,
    id?: string
}

export function SavePivotValidator(request: SavePivot): ValidationResult {
    const err: string[] = [];
    if (IsNullOrEmptyOrWhitespace(request.title)) err.push("title is required");       
    //if(request.data.length==0) err.push("datasource is required");
    const success = err.length == 0;
    return { errors: err, success: success };
}
import { IMessageRouter } from "../core/abstractions/routing";
import { Container } from "inversify";
import { IPivotRepository } from "./domain/repositories/pivot";
import { PivotRepository } from "./repositories/pivot";
import { SavePivotRoute, RemovePivotRoute, PivotListRoute, PivotDetailRoute, PivotQueriesRoute } from "./messages/routes";
import { SavePivotHandlerFactory } from "./handlers/command/savePivot";
import { Role } from "../core/abstractions/access";
import { IHttpContext } from "../core/abstractions/appcontext";
import { AppContextType } from "../core/abstractions/dependecyTypes";
import { SavePivotValidator } from "./messages/command/savePivot";
import { RemovePivotHandlerFactory } from "./handlers/command/removePivot";
import { RemovePivotValidator } from "./messages/command/removePivot";
import { PivotListHandlerFactory } from "./handlers/query/pivotList";
import { PivotDetailHandlerFactory } from "./handlers/query/pivotDetail";
import { PivotDetailValidator } from "./messages/query/pivotDetail";
import { PivotQueriesHandlerFactory } from "./handlers/query/pivotQueries";
import { PivotQueriesValidator } from "./messages/query/pivotQueries";

function getPivotRepository(container: Container): IPivotRepository {
    return container.get<IPivotRepository>("IPivotRepository");
}

export function RegisterPivotRouters(container: Container, router: IMessageRouter): void {
    router.registerCommandHandler(SavePivotRoute,
        SavePivotHandlerFactory(
            getPivotRepository(container),
            () => container.get<IHttpContext>(AppContextType)),
        SavePivotValidator, [Role.Master, Role.Admin, Role.Designer]);

    router.registerCommandHandler(RemovePivotRoute,
        RemovePivotHandlerFactory(
            getPivotRepository(container),
            () => container.get<IHttpContext>(AppContextType)),
        RemovePivotValidator, [Role.Master, Role.Admin, Role.Designer]);

    router.registerCommandHandler(PivotListRoute,
        PivotListHandlerFactory(
            getPivotRepository(container),
            () => container.get<IHttpContext>(AppContextType)),
        undefined, [Role.Master, Role.Admin, Role.Designer]);

    router.registerCommandHandler(PivotDetailRoute,
        PivotDetailHandlerFactory(
            getPivotRepository(container)),
        PivotDetailValidator, [Role.Master, Role.Admin, Role.Designer, Role.Viewer]);

    router.registerCommandHandler(PivotQueriesRoute,
        PivotQueriesHandlerFactory(
            getPivotRepository(container)),
        PivotQueriesValidator, [Role.Master, Role.Admin, Role.Designer, Role.Viewer]);
}

export function RegisterPivotTypes(container: Container): void {
    container.bind<IPivotRepository>("IPivotRepository").to(PivotRepository).inSingletonScope();
}
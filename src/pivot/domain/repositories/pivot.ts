import { Pivot } from "../models/pivot";
import { QueryInfo } from "../models/queryInfo";

export interface IPivotRepository {
    save(data: Pivot): Promise<boolean>;
    remove(id:string):Promise<boolean>;
    findById(id: string): Promise<Pivot | null>;
    findByCustomerId(customerid: string): Promise<Pivot[]>;
    findByIds(ids: string[]): Promise<Pivot[]>;
    getQueries(ids:string[]):Promise<QueryInfo[]>;
}
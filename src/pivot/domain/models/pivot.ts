export interface DataSource {
    id: string,
    query: any[]
}
export interface Pivot {
    _id: string,
    title: string,
    createdBy:string,
    createdOn:string,
    updatedBy?:string,
    updatedOn?:string,
    creatorid:string,    
    data: DataSource[],    
    customerid:string,
    options: any,
}

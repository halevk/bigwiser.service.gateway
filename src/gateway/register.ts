import { Container } from "inversify"
import { IMessageRouter } from "../core/abstractions/routing";
// Adapters
import { RegisterMessagingTypes } from "../adapters/messaging/register";
import { RegisterRepositoryTypes } from "../adapters/repository/register";
import { RegisterCachingTypes } from "../adapters/caching/register";
import { RegisterTokenTypes } from "../adapters/jwtprovider/register";
import { RegisterLoggerTypes, RegisterLoggingRouters } from "../adapters/logging/register";
import { RegisterContextTypes } from "../adapters/context/register";
// Widget
import { RegisterWidgetRouters, RegisterWidgetTypes } from "../widget/register";
// Dashboard
import { RegisterDashboardRouters, RegisterDashboardTypes } from "../dashboard/register";
//Auth
import { RegisterAuthTypes, RegisterAuthRouters } from "../auth/register";
// Notification
import { RegisterNotificationTypes, RegisterNotificationRouters } from "../notification/register";
import { IOptions } from "./options";
import { MessageRouterType } from "../core/abstractions/dependecyTypes";
// Dataset
import { RegisterDatasetTypes, RegisterDatasetRouters } from "../dataset/register";
import { RegisterDataRouters } from "../data/register";
import { RegisterCustomerRouters, RegisterCustomerTypes } from "../customer/register";
import { RegisterDruidTypes } from "../adapters/druid/register";
// Pivot 
import { RegisterPivotTypes, RegisterPivotRouters } from "../pivot/register";


export function RegisterTypes(options: IOptions): Container {
    const container = new Container();
    RegisterContextTypes(container);
    RegisterMessagingTypes(container);
    RegisterRepositoryTypes(options.repositoryOptions, container);
    RegisterCachingTypes(options.cacheOptions, container);
    RegisterTokenTypes(options.tokenOptions, container);
    RegisterAuthTypes(container);
    RegisterLoggerTypes(options.loggerOptions, container);
    RegisterNotificationTypes(options.notificationOptions, container);
    RegisterDatasetTypes(container);
    RegisterDashboardTypes(container);
    RegisterWidgetTypes(container);  
    RegisterPivotTypes(container);  
    RegisterCustomerTypes(container);
    RegisterDruidTypes(options.dataOptions,container);
    return container;
}

export function RegisterRoutes(container: Container) {
    const routers = container.get<IMessageRouter>(MessageRouterType);
    RegisterWidgetRouters(container, routers);
    RegisterDashboardRouters(container, routers);
    RegisterNotificationRouters(container, routers);
    RegisterAuthRouters(container, routers);
    RegisterDatasetRouters(container, routers);
    RegisterDashboardRouters(container, routers);
    RegisterWidgetRouters(container, routers);
    RegisterPivotRouters(container, routers);
    RegisterDataRouters(container, routers);
    RegisterCustomerRouters(container, routers);
    RegisterLoggingRouters(container,routers);
}

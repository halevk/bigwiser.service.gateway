require("dotenv").config();
import cluster from "cluster";
import 'reflect-metadata';
import cors from "cors";
import express from "express";
import bodyParser from "body-parser";
import axios from "axios";
axios.defaults.timeout = 1000 * 60 * 4;

const port = 11000;
const tokenKey = process.env.token_key || "";
const tokenDuration = process.env.token_duration || "10h";
const contentDbUrl = process.env.content_db_url || "";
const contentDbName = process.env.content_db_name || "";
const logDbUrl = process.env.log_db_url || "";
const logDbName = process.env.log_db_name || "";
const cacheHost = process.env.cache_host || "";
const cachePort = parseInt(process.env.cache_port || "6379");
const cachePwd = process.env.cache_pwd || "";
const emailService = process.env.email_server || "";
const emailUser = process.env.email_user || "";
const emailPassword = process.env.email_password || "";
const emailSender = process.env.email_sender || "";
const druidUrl = process.env.druid_url || "";

import { RegisterRoutes, RegisterTypes } from "./register";

import authMiddleware from "./middlewares/auth";
import methodMiddleware from "./middlewares/httpmethod";
import genericHandler from "./middlewares/genericHandler";
import { IOptions } from './options';
import { MongoClient } from 'mongodb';

const httpContext = require("express-http-context");
const contentDbConnect = new MongoClient(contentDbUrl,
    { useNewUrlParser: true, useUnifiedTopology: true }).connect();
const logDbConnect = new MongoClient(logDbUrl,
    { useNewUrlParser: true, useUnifiedTopology: true }).connect();

Promise.all([contentDbConnect, logDbConnect])
    .then(([contentClient, logClient]) => {
        const options: IOptions = {
            cacheOptions: { host: cacheHost, port: cachePort,pwd:cachePwd },
            loggerOptions: { dbName: logDbName, client: logClient },
            repositoryOptions: { dbName: contentDbName, client: contentClient },
            tokenOptions: { key: tokenKey, expires: tokenDuration },
            notificationOptions: {
                emailOptions: {
                    service: emailService,
                    auth: { user: emailUser, pass: emailPassword },
                    sender: emailSender
                }
            },
            dataOptions: { url: druidUrl }
        };

        const container = RegisterTypes(options);
        RegisterRoutes(container);
        const app = express();
        app.use(cors());
        app.use(bodyParser.urlencoded({ extended: true }));
        app.use(bodyParser.json());
        app.use(methodMiddleware());
        app.use(httpContext.middleware);
        app.use(authMiddleware(tokenKey, container));
        app.use(genericHandler(container));
        var srv = app.listen(port, () => {
            console.log(`worker: ${process.pid} is running on port number ${port}`);
        });
        //srv.setTimeout(2.5*60*1000);
    }).catch((err)=>console.log("aaa "+ err));

process.on('unhandledRejection', (error) => {
    console.log(`unhandled rejection occured on worker  ${process.pid} with error: ${error}`);
});
process.on("uncaughtException", (err: Error) => {
    console.log(`uncaught exception occured on worker ${process.pid} with error : ${err}`);
});


// if (cluster.isMaster) {
//    console.log(`master process pid:${process.pid}`);
//     if(process.env.NODE_ENV=="live")  {
//        const cpuCount = require("os").cpus().length;
//        for(let i=0;i<cpuCount;i++) cluster.fork();
//    } else cluster.fork();
// } else {  

// }




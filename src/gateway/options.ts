import { IRepositoryOptions } from "../adapters/repository/options";
import { ICacheOptions } from "../adapters/caching/options";
import { ILoggerOptions } from "../adapters/logging/options";
import { ITokenOptions } from "../adapters/jwtprovider/options";
import { INotificationOptions } from "../notification/options";
import { IDataOptions } from "../core/abstractions/dataRequester";

export interface IOptions {
    repositoryOptions: IRepositoryOptions,
    cacheOptions: ICacheOptions,
    loggerOptions: ILoggerOptions,
    tokenOptions:ITokenOptions,
    notificationOptions:INotificationOptions,
    dataOptions:IDataOptions
}
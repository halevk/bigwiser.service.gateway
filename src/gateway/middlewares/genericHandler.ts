import { Container } from "inversify";
import { Request, Response, NextFunction } from "express";
import { IMessageBroker } from "../../core/abstractions/routing";
import { IHttpContext, IAppContext, AppContextTypes } from "../../core/abstractions/appcontext";
import { AppContextType } from "../../core/abstractions/dependecyTypes";


export default (container: Container) => {
    const broker = container.get<IMessageBroker>("IMessageBroker");
    const ctx = container.get<IHttpContext>(AppContextType);
    return async (req: Request, resp: Response, next: NextFunction) => {        
        const route = req.url;
        const result = await broker.send(route, req.body, true);
        const appCtx = ctx.Get<IAppContext>(AppContextTypes.Context);
        if(appCtx)
            resp.setHeader("RequestId", appCtx.requestId);        
        resp.send(result);
    }
}
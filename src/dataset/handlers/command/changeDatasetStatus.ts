import { ChangeDatasetStatus, ChangeDatasetStatusResponse } from "../../messages/command/changeDatasetStatus";
import { IDatasetRepository } from "../../domain/repositories/dataset";

export function ChangeDatasetStatusHandlerFactory(
    repo: IDatasetRepository) {
    return async (req: ChangeDatasetStatus): Promise<ChangeDatasetStatusResponse> => {       
        const ds = await repo.findById(req.id);
        if (ds == null) return { success: false, msg: "dataset not found" };
        ds.enable = req.status;
        const result = await repo.save(ds);
        if (!result) return { success: false, msg: "status could not changed" };
        return { success: true, msg: "status changed" };
    }
}
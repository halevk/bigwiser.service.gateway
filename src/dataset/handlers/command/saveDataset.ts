import { IDatasetRepository } from "../../domain/repositories/dataset";
import { SaveDataset, SaveDatasetResponse } from "../../messages/command/saveDataset";
import { IsNullOrEmptyOrWhitespace } from "../../../core/extensions/stringextensions";
import { Dataset } from "../../domain/models/dataset";
const uuid = require("uuid/v4");

export function SaveDatasetHandlerFactory(repo: IDatasetRepository) {
    return async (req: SaveDataset): Promise<SaveDatasetResponse> => {
        const id = IsNullOrEmptyOrWhitespace(req.id) ? uuid() : req.id;
        const ds: Dataset = {
            _id: id,
            enable: req.enable,
            fields: req.fields,
            name: req.name,
            type: req.type,
            alias: IsNullOrEmptyOrWhitespace(req.alias) ? req.name : req.alias,
            default: req.default
        };        
        const result = repo.save(ds);
        if (!result) return { success: false, msg: "could not saved" };
        return { success: true, msg: "saved succesfully" };
    }
}
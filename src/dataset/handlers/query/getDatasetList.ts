import { IDatasetRepository } from "../../domain/repositories/dataset";
import {  DatasetInfoWihtoutField } from "../../messages/common/basicDatasetInfo";
import { HttpContextFactory, IAppContext, AppContextTypes } from "../../../core/abstractions/appcontext";
import { IsUserMemberOf } from "../../../core/extensions/role";
import { Role } from "../../../core/abstractions/access";
import { Dataset } from "../../domain/models/dataset";
import { GetDatasetList, GetDatasetListResponse } from "../../messages/query/getDatasetList";

export function GetDatasetListHandlerFactory(
    repo: IDatasetRepository,
    contextFactory: HttpContextFactory) {
    return async (req: GetDatasetList): Promise<GetDatasetListResponse> => {
        const ctx = contextFactory().Get<IAppContext>(AppContextTypes.Context);
        let datasets: Dataset[] = [];       
        if (IsUserMemberOf(ctx.roles, [Role.Master]))
            datasets = await repo.findAll();
        else if (ctx.customerInfo != undefined)
            datasets = await repo.findByIds(ctx.customerInfo.datasets.map(p => p.id));
        if (datasets.length == 0) return { datasets: [] };
        const mapped = datasets.map(p => {
            const item: DatasetInfoWihtoutField = {
                id: p._id,
                name: p.name,
                alias: p.alias,
                enabled: p.enable
            }
            return item;
        });
        return { datasets: mapped };
    }
}
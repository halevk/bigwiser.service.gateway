import { IDatasetRepository } from "../../domain/repositories/dataset";
import { GetCustomerDatasets, GetCustomerDatasetsResponse } from "../../messages/query/getCustomerDatasets";
import { BasicDatasetInfo } from "../../messages/common/basicDatasetInfo";
import { FieldInfo } from "../../messages/common/fieldInfo";
import { HttpContextFactory, IAppContext, AppContextTypes } from "../../../core/abstractions/appcontext";
import { IsUserMemberOf } from "../../../core/extensions/role";
import { Role } from "../../../core/abstractions/access";
import { Dataset } from "../../domain/models/dataset";

export function GetCustomerDatasetsHandlerFactory(
    repo: IDatasetRepository,
    contextFactory: HttpContextFactory) {
    return async (req: GetCustomerDatasets): Promise<GetCustomerDatasetsResponse> => {
        const ctx = contextFactory().Get<IAppContext>(AppContextTypes.Context);
        let datasets: Dataset[] = [];       
        if (IsUserMemberOf(ctx.roles, [Role.Master]))
            datasets = await repo.findAll();
        else if (ctx.customerInfo != undefined)
            datasets = await repo.findByIds(ctx.customerInfo.datasets.map(p => p.id));
        if (datasets.length == 0) return { datasets: [] };
        const mapped = datasets.map(p => {
            const item: BasicDatasetInfo = {
                id: p._id,
                name: p.name,
                alias: p.alias,
                enabled: p.enable,
                fields: p.fields.filter(p => !p.hidden).map(o => {
                    const fin: FieldInfo = { name: o.name, type: o.type, alias: o.alias };
                    return fin;
                })
            }
            return item;
        });
        return { datasets: mapped };
    }
}
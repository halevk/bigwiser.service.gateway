import { Container } from "inversify/dts/container/container";
import { IMessageRouter } from "../core/abstractions/routing";
import { IDatasetRepository } from "./domain/repositories/dataset";
import { DatasetRepository } from "./repositories/datasetRepository";
import {
    GetCustomerDatasetsRoute,
    ChangeDatasetStatusRoute,
    SaveDatasetRoute,
    ListDatasetRoute,
    GetFieldsDefinitionRoute,
    ListDatasetFieldsRoute
} from "./messages/routes";
import { GetCustomerDatasetsHandlerFactory } from "./handlers/query/getCustomerDatasets";
import { IHttpContext } from "../core/abstractions/appcontext";
import { AppContextType } from "../core/abstractions/dependecyTypes";
import { Role } from "../core/abstractions/access";
import { ChangeDatasetStatusValidator } from "./messages/command/changeDatasetStatus";
import { ChangeDatasetStatusHandlerFactory } from "./handlers/command/changeDatasetStatus";
import { SaveDatasetHandlerFactory } from "./handlers/command/saveDataset";
import { SaveDatasetValidator } from "./messages/command/saveDataset";
import { GetDatasetListHandlerFactory } from "./handlers/query/getDatasetList";
import { GetFieldsDefnitionHandlerFactory } from "./handlers/query/getFieldsDefinition";
import { GetFieldsDefinitionValidator } from "./messages/query/getFieldsDefinition";
import { IDataRequester } from "../core/abstractions/dataRequester";
import { GetDatasetFieldsHandlerFactory } from "./handlers/query/getDatasetFields";
import { GetDatasetFieldsValidator } from "./messages/query/getDatasetFields";

function getDatasetRepository(container: Container): IDatasetRepository {
    return container.get<IDatasetRepository>("IDatasetRepository");
}

function getDataRequester(container: Container): IDataRequester {
    return container.get<IDataRequester>("IDataRequester");
}

export function RegisterDatasetRouters(container: Container, router: IMessageRouter): void {
    router.registerCommandHandler(GetCustomerDatasetsRoute,
        GetCustomerDatasetsHandlerFactory(
            getDatasetRepository(container),
            () => container.get<IHttpContext>(AppContextType)),
        undefined, [Role.Admin, Role.Master, Role.Designer]);

    router.registerCommandHandler(ListDatasetFieldsRoute,
        GetDatasetFieldsHandlerFactory(
            getDatasetRepository(container),
            () => container.get<IHttpContext>(AppContextType)),
            GetDatasetFieldsValidator, [Role.Admin, Role.Master, Role.Designer]);

    router.registerCommandHandler(GetFieldsDefinitionRoute,
        GetFieldsDefnitionHandlerFactory(
            getDataRequester(container),
            getDatasetRepository(container)),
        GetFieldsDefinitionValidator, [Role.Master]);

    router.registerCommandHandler(ListDatasetRoute,
        GetDatasetListHandlerFactory(
            getDatasetRepository(container),
            () => container.get<IHttpContext>(AppContextType)),
        undefined, [Role.Admin, Role.Master, Role.Designer]);

    router.registerCommandHandler(ChangeDatasetStatusRoute,
        ChangeDatasetStatusHandlerFactory(getDatasetRepository(container)),
        ChangeDatasetStatusValidator, [Role.Master]);

    router.registerCommandHandler(SaveDatasetRoute,
        SaveDatasetHandlerFactory(getDatasetRepository(container)),
        SaveDatasetValidator, [Role.Master]);
}

export function RegisterDatasetTypes(container: Container): void {
    container.bind<IDatasetRepository>("IDatasetRepository").to(DatasetRepository).inSingletonScope();
}
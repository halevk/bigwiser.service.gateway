import { Dataset } from "../models/dataset";

export interface IDatasetRepository {
    findByIds(ids: string[]): Promise<Dataset[]>;
    findById(id: string): Promise<Dataset | null>;
    findAll():Promise<Dataset[]>;
    save(data: Dataset): Promise<boolean>;
}
import { ValidationResult } from "../../../core/abstractions/routing";
import { IsNullOrEmptyOrWhitespace } from "../../../core/extensions/stringextensions";

export class ChangeDatasetStatus {
    id:string="";
    status:boolean=false;
}

export interface ChangeDatasetStatusResponse{
    success:boolean,
    msg:string
}

export function ChangeDatasetStatusValidator(request: ChangeDatasetStatus): ValidationResult {
    const err: string[] = [];
    if (IsNullOrEmptyOrWhitespace(request.id)) err.push("id is required");   
    const success = err.length == 0;
    return { errors: err, success: success };

}
import { ValidationResult } from "../../../core/abstractions/routing";
import { IsNullOrEmptyOrWhitespace } from "../../../core/extensions/stringextensions";
import { FieldWithHiddenInfo } from "../common/fieldInfo";

export class SaveDataset {
    id: string = "";
    name: string = "";
    enable: boolean = false;
    fields: FieldWithHiddenInfo[] = [];   
    type: string = "";
    alias: string = "";
    default:boolean=false;
}

export interface SaveDatasetResponse {
    success: boolean,
    msg: string
}

export function SaveDatasetValidator(request: SaveDataset): ValidationResult {
    const err: string[] = [];
    if (IsNullOrEmptyOrWhitespace(request.name)) err.push("name is required");
    else if (request.name.indexOf(" ") < -1) err.push("name can not contain empty space");
    if (IsNullOrEmptyOrWhitespace(request.type)) err.push("type is required");
    if (request.fields.length == 0) err.push("fields are required");
    const success = err.length == 0;
    return { errors: err, success: success };

}
import { FieldInfo } from "./fieldInfo";

export interface BasicDatasetInfo {
    id:string,
    name:string,   
    alias:string,
    enabled:boolean,
    fields:FieldInfo[]
}

export interface DatasetInfoWihtoutField {
    id:string,
    name:string,   
    alias:string,
    enabled:boolean    
}
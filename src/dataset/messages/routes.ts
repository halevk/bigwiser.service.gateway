export const GetCustomerDatasetsRoute = "/dataset/ofcustomer";
export const ChangeDatasetStatusRoute = "/dataset/changestatus";
export const SaveDatasetRoute = "/dataset/save";
export const ListDatasetRoute = "/dataset/list";
export const GetFieldsDefinitionRoute="/dataset/metadata";
export const ListDatasetFieldsRoute="/dataset/fields";
import { SimpleFieldInfo } from "../common/fieldInfo";
import { ValidationResult } from "../../../core/abstractions/routing";
import { IsNullOrEmptyOrWhitespace } from "../../../core/extensions/stringextensions";

export class GetFieldsDefinition {
    id:string="";
}

export interface GetFieldsDefinitionResponse {
    fields:SimpleFieldInfo[]
}

export function GetFieldsDefinitionValidator(request: GetFieldsDefinition): ValidationResult {
    const err: string[] = [];
    if (IsNullOrEmptyOrWhitespace(request.id)) err.push("id is required");    
    const success = err.length == 0;
    return { errors: err, success: success };

}

import { FieldWithHiddenInfo } from "../common/fieldInfo";
import { ValidationResult } from "../../../core/abstractions/routing";
import { IsNullOrEmptyOrWhitespace } from "../../../core/extensions/stringextensions";

export class GetDatasetFields {
    id:string="";
}

export interface GetDatasetFieldsResponse {
    fields:FieldWithHiddenInfo[]
}

export function GetDatasetFieldsValidator(request: GetDatasetFields): ValidationResult {
    const err: string[] = [];
    if (IsNullOrEmptyOrWhitespace(request.id)) err.push("id is required");    
    const success = err.length == 0;
    return { errors: err, success: success };

}

import { IDatasetRepository } from "../domain/repositories/dataset";
import { IRepository } from "../../core/abstractions/repository";
import { RepositoryType } from "../../core/abstractions/dependecyTypes";
import { inject, injectable } from "inversify";
import { Dataset } from "../domain/models/dataset";

@injectable()
export class DatasetRepository implements IDatasetRepository {
    

    private repository: IRepository
    collectionName: string = "datasets";
    constructor(@inject(RepositoryType) repo: IRepository) {
        this.repository = repo;
    }

    findByIds(ids: string[]): Promise<Dataset[]> {
        return this.repository.findMultipleByQuery<Dataset>(this.collectionName, { "_id": { "$in": ids }, "enable": true });
    }

    findById(id: string): Promise<Dataset | null> {
        return this.repository.findOneById<Dataset>(this.collectionName, id)
    }
    save(data: Dataset): Promise<boolean> {
        return this.repository.save(this.collectionName, data._id, data);
    }

    findAll(): Promise<Dataset[]> {
        return this.repository.findMultipleByQuery(this.collectionName,{});
    }

}
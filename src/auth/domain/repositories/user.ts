import { User } from "../models/user";

export interface IUserRepository {
    findByUserId(userid:string):Promise<User | null>;
    findByEmail(email:string):Promise<User|null>; 
    listByCustomerId(customerId:string):Promise<User[]>;
    save(user:User):Promise<boolean>;
    remove(userid:string):Promise<boolean>;
    userExistsInGroup(customerid:string,groupid:string):Promise<boolean>;
    usersInGroup(groupid:string):Promise<User[]>;
}
import { DatasetInfo } from "../../../core/abstractions/appcontext";

export interface User {
    _id: string,
    name: string,
    surname: string,
    email: string,
    pwd: string,
    groups: string[],
    customerid: string,
    datasets?: DatasetInfo[], // only for designers
    isEnabled: boolean,
    lastLogOut: string,
    roles: string[]
}


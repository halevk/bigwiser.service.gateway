import { ChangeUserDatasetAccess, ChangeUserDatasetAccessResponse } from "../../messages/command/changeUserDatasetAccess";
import { IUserRepository } from "../../domain/repositories/user";
import { HttpContextFactory, AppContextTypes, IAppContext } from "../../../core/abstractions/appcontext";

export function ChangeUserDatasetAccessHandlerFactory(
    userRepo: IUserRepository,
    factory: HttpContextFactory){
    return async(req:ChangeUserDatasetAccess):Promise<ChangeUserDatasetAccessResponse> =>{
        const ctx = factory().Get<IAppContext>(AppContextTypes.Context);
        const user = await userRepo.findByUserId(req.id);
        if(!user) return {success:false,errors:["user not found"]};
        if(ctx.customerid!=user.customerid) return {success:false,errors:["you are not authorized"]};
        user.datasets = req.datasets;
        const isSuccess = await userRepo.save(user);
        if(!isSuccess) return {success:false,errors:["user did not saved"]};
        return {success:true,errors:[]};
    }
}
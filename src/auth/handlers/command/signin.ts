import { SignIn, SignInResponse } from "../../messages/command/signin";
import { IUserRepository } from "../../domain/repositories/user";
import bcrypt from "bcryptjs";
import { ITokenProvider } from "../../../core/abstractions/token";
import { ICache } from "../../../core/abstractions/caching";
import { AuthErrorMessages } from "../errors";
import { IMessageBroker } from "../../../core/abstractions/routing";

export function SignInHandlerFactory(
    broker:IMessageBroker,
    userRepo: IUserRepository, 
    tokenProvider: ITokenProvider, 
    cache: ICache) {
    return async (req: SignIn): Promise<SignInResponse> => {
        const user = await userRepo.findByEmail(req.email);        
        if (user == null) return { success: false, errors: [AuthErrorMessages.InvalidCredentials] };      
        const result = await bcrypt.compare(req.pwd, user.pwd);        
        if (!result) return { success: false, errors: [AuthErrorMessages.InvalidCredentials] };
        if (!user.isEnabled) return { success: false, errors: [AuthErrorMessages.AccountDisabled] };
        const token = await tokenProvider.Sign({ id: user._id, name: user.name,r:user.roles.map(p=> p[0]) });
        broker.publish("auth:user:signin",{
            user:`${user.name} ${user.surname}`,
            action:"signin",
            date:new Date()
        })
        // const cacheResult = await cache.setSingle<User>(`user-${user._id}`, user);
        // if (!cacheResult) return { success: false, errors: ["An error occured please try again"], token: "" };
        return { success: true, errors: [], token: token };
    }
}
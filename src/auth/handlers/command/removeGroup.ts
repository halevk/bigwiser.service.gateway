import { RemoveGroup, RemoveGroupResponse } from "../../messages/command/removeGroup";
import { IGroupRepository } from "../../domain/repositories/group";
import { IUserRepository } from "../../domain/repositories/user";
import { IAppContext, AppContextTypes, HttpContextFactory } from "../../../core/abstractions/appcontext";

export function RemoveGroupHandlerFactory(
    groupRepo: IGroupRepository,
    userRepo: IUserRepository,
    factory: HttpContextFactory) {
    return async (req: RemoveGroup): Promise<RemoveGroupResponse> => {
        const ctx = factory().Get<IAppContext>(AppContextTypes.Context);
        const userExists = await userRepo.userExistsInGroup(ctx.customerid, req.id);
        if (userExists) return { success: false, errors: ["There are users in this group"] };        
        const result = await groupRepo.remove(req.id);
        if (!result) return { success: false, errors: ["Group could not removed"] }
        return { success: true, errors: [] }
    }
}
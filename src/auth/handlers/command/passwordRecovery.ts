import { IUserRepository } from "../../domain/repositories/user";
import { PasswordRecovery, PasswordRecoveryResponse } from "../../messages/command/passwordRecovery";
import bcrypt from "bcryptjs";
import { GenerateRandomPwd } from "../../domain/extensions/pwd";
import { IMessageBroker } from "../../../core/abstractions/routing";
import { SendEmailResponse } from "../../../notification/messages/command/sendemail";
import { SendEmailRoute } from "../../../notification/messages/routes";

export function PasswordRecoveryHandlerFactory (userRepo: IUserRepository, broker: IMessageBroker) {
    return async (request: PasswordRecovery): Promise<PasswordRecoveryResponse> => {
        const user = await userRepo.findByEmail(request.email);
        if (user == null) return { success: false, msg: "email not found" };
        const newPwd = GenerateRandomPwd(10);
        const newHashedPwd = await bcrypt.hash(newPwd, 10);
        user.pwd = newHashedPwd;
        const saveResult = await userRepo.save(user);        
        if (saveResult) {
            const emailContent = `
                <div>
                <h2>Dear ${user.name} ${user.surname}</h2>
                <div>Your New Password : ${newPwd}</div>
                <p>you can login from <a href="https://www.brings.com">here</a></p>
                </div>
            `;
            const msg: any = {
                subject: "Password Recovery",
                body: emailContent,
                cc: "",
                bcc: "",
                from: "support@brings.com",
                to: user.email,
                bodyinhtml: true
            };
            const result = await broker.send(SendEmailRoute, msg) as SendEmailResponse;
            if(!result.success) return {success:false,msg:result.error};
            else return {success:true,msg:""};
        }
        return {success:false,msg:"new password could not be saved"};        
    }
}
import { ChangePassword, ChangePasswordResponse } from "../../messages/command/changePassword";
import { IUserRepository } from "../../domain/repositories/user";
import { IAppContext, AppContextTypes, HttpContextFactory } from "../../../core/abstractions/appcontext";
import bcrypt from "bcryptjs";

export function ChangePasswordHandlerFactory (userRepository: IUserRepository,factory:HttpContextFactory) {
    return async (request: ChangePassword): Promise<ChangePasswordResponse> => {
        const ctx = factory().Get<IAppContext>(AppContextTypes.Context);
        const user = await userRepository.findByUserId(ctx.userId);
        if (user == null) return { success: false, msg: "user not found" };
        const hashedOldPwd = await bcrypt.hash(request.oldpwd, 10);
        if (user.pwd != hashedOldPwd) return { success: false, msg: "your old password did not matched" };
        const newHashedPwd = await bcrypt.hash(request.retypednewpwd, 10);
        user.pwd = newHashedPwd;
        const saveResult = await userRepository.save(user);
        if (!saveResult) return { success: false, msg: "new password did not saved" }
        return { success: true, msg: "password successfully changed" };
    }
}
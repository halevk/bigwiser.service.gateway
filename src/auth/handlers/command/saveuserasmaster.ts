import { SaveUserAsMaster, SaveUserAsMasterResponse } from "../../messages/command/saveuserasmaster";
import { IUserRepository } from "../../domain/repositories/user";
import { User } from "../../domain/models/user";
const uuidv4 = require("uuid/v4");
import bcrypt from "bcryptjs";
import { IsNullOrEmptyOrWhitespace } from "../../../core/extensions/stringextensions";

export function SaveUserAsMasterHandlerFactory(
    userRepo: IUserRepository) {
    return async (req: SaveUserAsMaster): Promise<SaveUserAsMasterResponse> => {        
        const isNew = IsNullOrEmptyOrWhitespace(req.id);
        const id = isNew ? uuidv4() : req.id;
        let user: User | null = null;
        const usrExists = await userRepo.findByEmail(req.email);
        if (usrExists != null && usrExists._id != req.id)
            return { success: false, msg: ["email is used by another user"] };
        if (isNew) {
            const hashedPwd = await bcrypt.hash(req.pwd, 10);
            user = {
                _id: id,
                customerid: req.customerid,
                email: req.email,
                groups: req.groups,
                isEnabled: true,
                name: req.name,
                pwd: hashedPwd,
                surname: req.surname,
                lastLogOut: "",
                roles: req.roles,
                datasets: req.datasets,
            };
        } else {
            user = await userRepo.findByUserId(req.id);
            if (user == null) return { success: false, msg: ["user not found"] };
            user.email = req.email;
            user.customerid = req.customerid;
            user.isEnabled = req.isEnabled;
            user.name = req.name;
            user.surname = req.surname;
            user.roles = req.roles;
            user.datasets = req.datasets;
        }
        const result = await userRepo.save(user);
        if (!result) return { success: false, msg: ["user not saved"] }
        return { success: true, msg: undefined }
    }
}
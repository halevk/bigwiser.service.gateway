import { ChangeUserPwdAsAdmin, ChangeUserPwdAsAdminResponse } from "../../messages/command/changeUserPwdAsAdmin";
import { HttpContextFactory, AppContextTypes, IAppContext } from "../../../core/abstractions/appcontext";
import { IUserRepository } from "../../domain/repositories/user";
import bcrypt from "bcryptjs";

export function ChangeUserPwdAsAdminHandlerFactory(
    userRepo: IUserRepository,
    factory: HttpContextFactory
) {
    return async (req: ChangeUserPwdAsAdmin): Promise<ChangeUserPwdAsAdminResponse> => {
        const ctx = factory().Get<IAppContext>(AppContextTypes.Context);
        const user = await userRepo.findByUserId(req.id);
        if(!user) return {success:false,errors:["user not found"]};
        if(ctx.customerid!=user.customerid) return {success:false,errors:["you are not authorized"]};
        const hashedPwd = await bcrypt.hash(req.newpwd, 10);
        user.pwd = hashedPwd;
        const isSuccess = await userRepo.save(user);
        if(!isSuccess) return {success:false,errors:["user did not saved"]};
        return {success:true,errors:[]};
    }
}
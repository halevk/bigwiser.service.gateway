import { IUserRepository } from "../../domain/repositories/user";
import { ChangeUserStatus, ChangeUserStatusResponse } from "../../messages/command/changeUserStatus";

export function ChangeUserStatusHandlerFactory (userRepo:IUserRepository) {
    return async(req:ChangeUserStatus):Promise<ChangeUserStatusResponse> => {
        const user = await userRepo.findByUserId(req.id);
        if(user==null) return {success:false,msg:["user not found"]}
        user.isEnabled=req.active;
        const result = await userRepo.save(user);
        if(!result) return {success:false,msg:["user status could not changed"] }
        return {success:true,msg:["user status changed"]}
    }
}
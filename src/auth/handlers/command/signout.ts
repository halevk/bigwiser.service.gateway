import { SignOut, SignOutResponse } from "../../messages/command/signout";
import { IUserRepository } from "../../domain/repositories/user";
import { IAppContext, AppContextTypes, HttpContextFactory } from "../../../core/abstractions/appcontext";
import { GetUtcNowAsString } from "../../../core/extensions/date";
import { ICache } from "../../../core/abstractions/caching";
import { IMessageBroker } from "../../../core/abstractions/routing";

export function SignOutHandlerFactory (
    broker:IMessageBroker,
    userRepo: IUserRepository,
    contextFactory:HttpContextFactory,
    cache:ICache) {
    return async (request: SignOut): Promise<SignOutResponse> => {        
        const ctx = contextFactory().Get<IAppContext>(AppContextTypes.Context);
        const user = await userRepo.findByUserId(ctx.userId);
        if (user == null) return { success: false };
        user.lastLogOut = GetUtcNowAsString();
        const result = await userRepo.save(user);
        broker.publish("auth:user:signout",{
            user:`${user.name} ${user.surname}`,
            date:new Date(),
            action:"signout"
        })
        //cache.removeSingle(`user-${ctx.userId}`);
        return {success:result};
    }
}
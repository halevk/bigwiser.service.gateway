import { GetUsersInGroup, GetUsersInGroupResponse } from "../../messages/query/getUsersInGroup";
import { IUserRepository } from "../../domain/repositories/user";
import { BasicUserInfo } from "../../messages/common/basicUser";

export function GetUsersInGroupHandlerFactory(userRepo: IUserRepository) {
    return async (req: GetUsersInGroup): Promise<GetUsersInGroupResponse> => {
        const users = await userRepo.usersInGroup(req.groupid);
        if (users.length == 0) return { users: [] }
        const mappedUsers = users.map(p => {
            const item: BasicUserInfo = { id: p._id, name: p.name, surname: p.surname };
            return item;
        })
        return { users: mappedUsers };
    }
}
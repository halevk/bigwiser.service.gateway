import { GetUsersOfCustomer, GetUsersOfCustomerResponse } from "../../messages/query/getUsersOfCustomer";
import { IUserRepository } from "../../domain/repositories/user";
import {  IAppContext, AppContextTypes, HttpContextFactory } from "../../../core/abstractions/appcontext";
import { ModerateUserInfo } from "../../messages/common/basicUser";
import { IGroupRepository } from "../../domain/repositories/group";
import { Group } from "../../domain/models/group";
import { User } from "../../domain/models/user";

export function GetUsersOfCustomerHandlerFactory (
    userRepo: IUserRepository, 
    groupRepo:IGroupRepository,
    contextFactory: HttpContextFactory) {
    return async (req: GetUsersOfCustomer): Promise<GetUsersOfCustomerResponse> => {
        const ctx = contextFactory().Get<IAppContext>(AppContextTypes.Context);       
        const userTask = userRepo.listByCustomerId(ctx.customerid);
        const groupTask = groupRepo.listByCustomerId(ctx.customerid);          
        const [users,groups] = await Promise.all<User[],Group[]>([userTask,groupTask]);
        let groupDict:any={};
        if(groups)
            groups.forEach(p=> groupDict[p._id]=p.name);        
        if (users == null || users.length == 0) return { users: [] };
        const mappedUsers = users.map(p => {
            const item: ModerateUserInfo = { 
                id: p._id, 
                name: p.name, 
                surname: p.surname,
                email:p.email,
                groups : p.groups ,
                roles: p.roles,
                isEnabled:p.isEnabled
             };
            return item;
        });
        return { users: mappedUsers };
    }
}
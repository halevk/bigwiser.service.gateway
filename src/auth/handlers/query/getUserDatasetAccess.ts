import { GetUserDatasetAccess, GetUserDatasetAccessResponse } from "../../messages/query/getUserDatasetAccess";
import { HttpContextFactory, IAppContext, AppContextTypes } from "../../../core/abstractions/appcontext";
import { IUserRepository } from "../../domain/repositories/user";

export function GetUserDatasetAccessHandlerFactory(
    userRepo: IUserRepository,
    factory: HttpContextFactory
){
    return async(req:GetUserDatasetAccess):Promise<GetUserDatasetAccessResponse> => {
        const ctx = factory().Get<IAppContext>(AppContextTypes.Context);
        const user = await userRepo.findByUserId(req.id);
        if(!user) return {success:false,errors:["user not found"]};
        if(ctx.customerid!=user.customerid) return {success:false,errors:["you are not authorized"]};
        return {success:true,errors:[],datasets:user.datasets}
    }
}
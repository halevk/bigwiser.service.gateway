import { IGroupRepository } from "../../domain/repositories/group";
import { GetGroups, GetGroupsResponse } from "../../messages/query/getGroups";
import { BasicGroup } from "../../messages/common/basicGroup";
import { HttpContextFactory, IAppContext, AppContextTypes } from "../../../core/abstractions/appcontext";

export function GetGroupsHandlerFactory(repo: IGroupRepository,factory:HttpContextFactory) {
    return async (req: GetGroups): Promise<GetGroupsResponse> => {
        const ctx = factory().Get<IAppContext>(AppContextTypes.Context);
        const grps = await repo.listByCustomerId(ctx.customerid);
        if (grps.length == 0) return { groups: [] };
        const mappedGroups = grps.map(p => {
            const bg: BasicGroup = { id: p._id, name: p.name };
            return bg;
        });
        return { groups: mappedGroups };
    }
}
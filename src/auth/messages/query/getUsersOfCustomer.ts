import { ModerateUserInfo } from "../common/basicUser";

export class GetUsersOfCustomer{}

export interface GetUsersOfCustomerResponse{    
    users:ModerateUserInfo[]
}


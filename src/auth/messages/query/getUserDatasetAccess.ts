import { ValidationResult } from "../../../core/abstractions/routing";
import { IsNullOrEmptyOrWhitespace } from "../../../core/extensions/stringextensions";
import { DatasetInfo } from "../../../core/abstractions/appcontext";

export class GetUserDatasetAccess {
    id:string="";   
}

export interface GetUserDatasetAccessResponse {
    success: boolean,
    errors: string[],
    datasets?: DatasetInfo[]   
}

export function GetUserDatasetAccessValidator(request: GetUserDatasetAccess): ValidationResult {
    const err: string[] = [];    
    if (IsNullOrEmptyOrWhitespace(request.id)) err.push("user is required");    
    return { errors: err, success: err.length == 0 };
}


import { ValidationResult } from "../../../core/abstractions/routing";
import { IsNullOrEmptyOrWhitespace } from "../../../core/extensions/stringextensions";
import { BasicUserInfo } from "../common/basicUser";

export class GetUsersInGroup{
    groupid:string="";    
}

export interface GetUsersInGroupResponse{    
    users:BasicUserInfo[]
}


export function GetUsersInGroupValidator(request:GetUsersInGroup):ValidationResult{
    const err:string[]=[];
    if(IsNullOrEmptyOrWhitespace(request.groupid)) err.push("group id is required");
    const success = err.length==0;
    return {errors:err,success:success};
}
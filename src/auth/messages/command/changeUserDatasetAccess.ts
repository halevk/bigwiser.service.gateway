import { DatasetInfo } from "../../../core/abstractions/appcontext";
import { ValidationResult } from "../../../core/abstractions/routing";
import { IsNullOrEmptyOrWhitespace } from "../../../core/extensions/stringextensions";

export class ChangeUserDatasetAccess {
    id:string="";
    datasets: DatasetInfo[] = [];
}

export interface ChangeUserDatasetAccessResponse {
    success: boolean,
    errors: string[]   
}

export function ChangeUserDatasetAccessValidator(request: ChangeUserDatasetAccess): ValidationResult {
    const err: string[] = [];    
    if (IsNullOrEmptyOrWhitespace(request.id)) err.push("user is required");    
    return { errors: err, success: err.length == 0 };
}


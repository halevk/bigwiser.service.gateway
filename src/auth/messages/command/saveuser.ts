import { ValidationResult } from "../../../core/abstractions/routing";
import { IsNullOrEmptyOrWhitespace } from "../../../core/extensions/stringextensions";

export class SaveUser {
    id: string = "";
    name: string = "";
    surname: string = "";
    email: string = "";
    pwd: string = "";
    repwd:string="";
    groups: string[] = [];
    roles: string[] = [];       
    isEnabled: boolean = false; 
}

export interface SaveUserResponse {
    success: boolean,
    errors: string[],
    id?: string
}


export function SaveUserValidator(request: SaveUser): ValidationResult {
    const err: string[] = [];    
    if (IsNullOrEmptyOrWhitespace(request.name)) err.push("name is required");
    if (IsNullOrEmptyOrWhitespace(request.surname)) err.push("surname is required");
    if (IsNullOrEmptyOrWhitespace(request.email)) err.push("email is required");
    if(request.id==""){
        if (IsNullOrEmptyOrWhitespace(request.pwd) ) err.push("password is required");
        if (IsNullOrEmptyOrWhitespace(request.repwd) ) err.push("re type pawword is required");
    }
    
    if(!IsNullOrEmptyOrWhitespace(request.pwd) && !IsNullOrEmptyOrWhitespace(request.repwd) && request.pwd!=request.repwd)
        err.push("passwords did not matched");        
    const success = err.length == 0;
    return { errors: err, success: success };

}
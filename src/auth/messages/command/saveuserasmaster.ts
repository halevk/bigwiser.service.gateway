import { ValidationResult } from "../../../core/abstractions/routing";
import { IsNullOrEmptyOrWhitespace } from "../../../core/extensions/stringextensions";
import { DatasetInfo } from "../../../core/abstractions/appcontext";

export class SaveUserAsMaster {
    id: string = "";
    name: string = "";
    surname: string = "";
    email: string = "";
    pwd: string = "";
    groups: string[] = [];
    roles: string[] = [];
    datasets: DatasetInfo[] = [];
    isEnabled: boolean = false;
    customerid: string = "";
}


export interface SaveUserAsMasterResponse {
    success: boolean,
    msg: string[] | undefined,
    id?: string
}


export function SaveUserAsMasterValidator(request: SaveUserAsMaster): ValidationResult {
    const err: string[] = [];
    if (IsNullOrEmptyOrWhitespace(request.name)) err.push("name is required");
    if (IsNullOrEmptyOrWhitespace(request.surname)) err.push("surname is required");
    if (IsNullOrEmptyOrWhitespace(request.email)) err.push("email is required");
    if (IsNullOrEmptyOrWhitespace(request.customerid)) err.push("customerid is required");
    if (IsNullOrEmptyOrWhitespace(request.pwd) && IsNullOrEmptyOrWhitespace(request.pwd)) err.push("password is required");
    const success = err.length == 0;
    return { errors: err, success: success };

}
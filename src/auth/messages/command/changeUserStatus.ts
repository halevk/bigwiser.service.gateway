import { ValidationResult } from "../../../core/abstractions/routing";
import { IsNullOrEmptyOrWhitespace } from "../../../core/extensions/stringextensions";

export class ChangeUserStatus {    
    id: string = "";
    active: boolean = true;   
}

export interface ChangeUserStatusResponse {
    success: boolean,
    msg: string[] | undefined,
}


export function ChangeUserStatusValidator(request: ChangeUserStatus): ValidationResult {
    const err: string[] = [];
    if (IsNullOrEmptyOrWhitespace(request.id)) err.push("id is required");    
    const success = err.length == 0;
    return { errors: err, success: success };

}
import { ValidationResult } from "../../../core/abstractions/routing";
import { IsNullOrEmptyOrWhitespace } from "../../../core/extensions/stringextensions";

export class RemoveGroup {    
    id: string = "";    
}

export interface RemoveGroupResponse {
    success: boolean,
    errors: string[] | undefined,
}


export function RemoveGroupValidator(request: RemoveGroup): ValidationResult {
    const err: string[] = [];
    if (IsNullOrEmptyOrWhitespace(request.id)) err.push("id is required");    
    const success = err.length == 0;
    return { errors: err, success: success };

}
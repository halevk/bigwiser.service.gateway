import { ValidationResult } from "../../../core/abstractions/routing";
import { IsNullOrEmptyOrWhitespace } from "../../../core/extensions/stringextensions";

export class SaveGroup {
    id:string="";    
    name: string = "";    
}

export interface SaveGroupResponse {
    success: boolean,
    errors: string[],
    id:string
}


export function SaveGroupValidator(request: SaveGroup): ValidationResult {
    const err: string[] = [];
    if (IsNullOrEmptyOrWhitespace(request.name)) err.push("name is required");   
    const success = err.length == 0;
    return { errors: err, success: success };

}
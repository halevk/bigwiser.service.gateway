import { ValidationResult } from "../../../core/abstractions/routing";
import { IsNullOrEmptyOrWhitespace } from "../../../core/extensions/stringextensions";

export class SignIn{
    email:string="";    
    pwd:string=""    
}

export interface SignInResponse{
    success:boolean,
    errors:string[],
    token?:string
}


export function SignInValidator(request:SignIn):ValidationResult{
    const err:string[]=[];
    if(IsNullOrEmptyOrWhitespace(request.email)) err.push("email is required");    
    if(IsNullOrEmptyOrWhitespace(request.pwd)) err.push("password is required");
    const success = err.length==0;
    return {errors:err,success:success};
}
import { ValidationResult } from "../../../core/abstractions/routing";
import { IsNullOrEmptyOrWhitespace } from "../../../core/extensions/stringextensions";

export class RemoveUser {    
    id: string = "";    
}

export interface RemoveUserResponse {
    success: boolean,
    msg: string[] | undefined,
}


export function RemoveUserValidator(request: RemoveUser): ValidationResult {
    const err: string[] = [];
    if (IsNullOrEmptyOrWhitespace(request.id)) err.push("id is required");    
    const success = err.length == 0;
    return { errors: err, success: success };

}
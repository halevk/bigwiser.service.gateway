import { ValidationResult } from "../../../core/abstractions/routing";
import { IsNullOrEmptyOrWhitespace } from "../../../core/extensions/stringextensions";

export class ChangeUserPwdAsAdmin {
    id:string="";
    newpwd:string="";
    renewpwd:string="";
}

export interface ChangeUserPwdAsAdminResponse {
    success: boolean,
    errors: string[]   
}

export function ChangeUserPwdAsAdminValidator(request: ChangeUserPwdAsAdmin): ValidationResult {
    const err: string[] = [];    
    if (IsNullOrEmptyOrWhitespace(request.id)) err.push("user is required");
    if (IsNullOrEmptyOrWhitespace(request.newpwd)) err.push("new password is required");
    if (IsNullOrEmptyOrWhitespace(request.renewpwd)) err.push("retype new password is required");    
    if(!IsNullOrEmptyOrWhitespace(request.newpwd) && 
    !IsNullOrEmptyOrWhitespace(request.renewpwd) &&
    request.newpwd!=request.renewpwd) err.push("passwords did not match");
    return { errors: err, success: err.length == 0 };
}


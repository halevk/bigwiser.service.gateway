export interface BasicUserInfo {
    id:string,
    name:string,
    surname:string
}

export interface ModerateUserInfo {
    id:string,
    name:string,
    surname:string,
    email:string,
    groups:string[],
    roles:string[],
    isEnabled:boolean
}
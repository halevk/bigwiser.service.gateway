export const SingInRoute = "/auth/signin";
export const SignOutRoute = "/auth/signout";
export const PasswordRecoveryRoute = "/auth/recoverpassword";

export const ChangePasswordRoute = "/user/changepwd";
export const ChangeUserGroupsRoute = "/user/changegroups";
export const ChangeUserStatusRoute = "/user/changestatus";
export const SaveUserRoute = "/user/save";
export const SaveUserAsMasterRoute = "/user/saveasmaster";
export const RemoveUserRoute = "/user/remove";
export const GetUsersInGroupRoute = "/user/ingroup";
export const GetUsersOfCustomerRoute = "/user/ofcustomer";
export const SaveUserRightRoute = "/user/right/save";

export const ChangeUserDataAccessRoute = "/user/dataset/save";
export const ChangeUserPwdAsAdminRoute = "/user/pwd/changeasadmin";
export const GetUserDatasetAccessRoute = "/user/get/datasetaccess";


export const SaveGroupRoute = "/group/save";
export const RemoveGroupRoute = "/group/remove";
export const GetGroupsRoute = "/group/list";


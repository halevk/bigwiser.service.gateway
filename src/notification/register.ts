import { Container } from "inversify";
import { IMessageRouter } from "../core/abstractions/routing";
import { ISmtpNotifier } from "./core/abstraction";
import { SmtpNotifier } from "./smtp/smtpNotifier";
import SendEmailHandler from "./handlers/command/sendemail";
import { CreateNewValidator } from "./messages/command/sendemail";
import {  SendEmailRoute } from "./messages/routes";
import { INotificationOptions, IEmailNotifyOptions } from "./options";

export function RegisterNotificationRouters(container: Container, router: IMessageRouter): void {
    router.registerCommandHandler(SendEmailRoute, SendEmailHandler(
        container.get<ISmtpNotifier>("IEmailNotifier")
    ), CreateNewValidator);

}

export function RegisterNotificationTypes(options:INotificationOptions,container: Container): void {
    container.bind<IEmailNotifyOptions>("IEmailNotifyOptions").toConstantValue(options.emailOptions)
    container.bind<ISmtpNotifier>("IEmailNotifier").to(SmtpNotifier).inSingletonScope();
}
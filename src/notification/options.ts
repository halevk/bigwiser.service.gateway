export interface IEmailNotifyAuthOptions {
    user:string,
    pass:string
}
export interface IEmailNotifyOptions {
    service:string,
    auth:IEmailNotifyAuthOptions,
    sender:string
}

export interface INotificationOptions {
    emailOptions:IEmailNotifyOptions
}
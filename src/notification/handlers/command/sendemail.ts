import { SendEmailResponse, SendEmail } from "../../messages/command/sendemail";
import { ISmtpNotifier, IEmailOptions } from "../../core/abstraction";

export default (sender: ISmtpNotifier) => {
    return async (req: SendEmail): Promise<SendEmailResponse> => {
        const msg: IEmailOptions = {
            from: req.from,
            to: req.to,
            bcc: req.bcc,
            cc: req.cc,
            body: req.body,
            isbodyhtml: req.bodyinhtml,
            subject: req.subject
        };
        const sendingReport = await sender.Send(msg);
        return {
            success: sendingReport.success,
            error: sendingReport.msg
        };
    }
}
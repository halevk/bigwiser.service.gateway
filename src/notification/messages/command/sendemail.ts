import { ValidationResult } from "../../../core/abstractions/routing";
import { IsNullOrEmptyOrWhitespace } from "../../../core/extensions/stringextensions";

export class SendEmail {
    subject: string = "";
    body: string = "";
    cc: string = "";
    bcc: string = "";
    from: string = "";
    to: string = "";
    bodyinhtml: boolean = false;
}

export interface SendEmailResponse {
    success: boolean,
    error: string
}


export function CreateNewValidator(request: SendEmail): ValidationResult {
    const err: string[] = [];
    if (IsNullOrEmptyOrWhitespace(request.subject)) err.push("subject is required");
    if (IsNullOrEmptyOrWhitespace(request.body)) err.push("body is required");
    if (request.to == "") err.push("receipents are required")
    const success = err.length == 0;
    return { errors: err, success: success };

}

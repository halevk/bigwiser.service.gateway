import { IAppContext, DatasetInfo, DatasetFieldInfo, CustomerInfo } from "../../core/abstractions/appcontext";
import { IsUserMemberOf } from "../../core/extensions/role";
import { Role } from "../../core/abstractions/access";

export function ApplyFilterItem(query: any, name: string, value: string | string[]) {
    const selector = (typeof (value) == "string") ? "selector" : "in";
    const filterExists: boolean = query.hasOwnProperty("filter");
    let itm:any ={type:selector,dimension:name};
    if(typeof(value)=="string") itm.value=value;
    else itm.values=value;
    if (!filterExists) {
        query.filter = itm;
        return;
    }

    if (query.filter.type == "and") {
        query.filter.fields.push(itm);
        return;
    }

    query.filter = {
        type: "and",
        fields: [itm, query.filter ]
    };
}


export function ApplyFilters(query: any, permissions: DatasetFieldInfo[]) {
    if (!permissions || permissions.length == 0) return;
    for (let itm of permissions) {
        if (itm.values.length == 1 && itm.values[0] == "*")
            continue;
        if (itm.values.length > 0)
            ApplyFilterItem(query, itm.name, itm.values);
    }
}

export function GetDataSourceName(query: any): string {
    const prop = "dataSource";
    if (!query.hasOwnProperty(prop)) return "";
    return query[prop];
}

export function GetDataset(name: string, customerInfo?: CustomerInfo): DatasetInfo | undefined {
    if (!customerInfo || customerInfo.datasets.length == 0) return undefined;
    return customerInfo.datasets.find(p => p.name == name);
}

export function GetPermissions(ds: DatasetInfo, ctx: IAppContext): DatasetFieldInfo[] { 
    if (IsUserMemberOf(ctx.roles, [Role.Master]) || !ctx.datasets )
        return ds.permissions;    

    let perms: DatasetFieldInfo[] = [];    
    if (ctx.datasets) {
        const usrDs = ctx.datasets.find(p => p.name == ds.name);
        if (usrDs && usrDs.permissions.length > 0)
            perms = usrDs.permissions;
    }
    // for (let itm of ds.permissions) {
    //     const field = perms.find(p => p.name == itm.name);
    //     if (!field) perms.push(itm);        
    // }
    return perms.concat(ds.permissions);
}
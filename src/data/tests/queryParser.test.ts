const parser = require("druid-sql-parser");
import { expect } from "chai";


describe("Sql Query Parser Test", function () {
  it("should_parse_simple_from", function () {
    const ast: any = parser.parse("Select * from Person");   
    expect(ast.from.value.table).to.be.equals("Person");
    expect(ast.selectParts[0].expr.type).to.be.equals("star");
    expect(ast.queryType).to.be.equals("SELECT");
  });

  it("should_parse_from_with_where", function () {
    const ast: any = parser.parse("SELECT accountroot,country, sum(searchcount) searchcount FROM dr_hsl GROUP BY accountroot,country ORDER BY searchcount desc LIMIT 10");  
    console.log(JSON.stringify(ast));
  });
});
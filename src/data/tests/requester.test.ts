// import mocha from "mocha";
// import { expect } from "chai";
// import { DruidRequester } from "../requesters/druidRequester";

// const url = "http://10.80.0.94:8082/druid/v2/?pretty";
// describe("Query Modify Tests", function () {
//     it("should_get_druid_fileds",function(){
//         const requester = DruidRequester({url:url});
//     });
    
//     it("should_get_data_from_druid", function () {
//         const requesterFn = DruidRequester({ url: url });
//         const query1 = {
//             "queryType": "timeseries",
//             "dataSource": "dr_pax_search_hotel",
//             "intervals": "2019-01-01T00:01Z/2019-12-31T00:01Z",
//             "granularity": "day",
//             "filter": {
//                 "type": "selector",
//                 "dimension": "name",
//                 "value": "Palmeras Beach Hotel"
//             },
//             "aggregations": [
//                 {
//                     "name": "__VALUE__",
//                     "type": "longSum",
//                     "fieldName": "searchcount"
//                 }
//             ]
//         };
//         var query2 = {
//             "queryType": "topN",
//             "dataSource": "dr_hsl",
//             "intervals": "2019-07-09T00:01Z/2019-07-10T00:01Z",
//             "granularity": "all",
//             "filter": {
//                 "type": "and",
//                 "fields": [
//                     {
//                         "type": "in",
//                         "dimension": "nationality",
//                         "values": [
//                             "RU",
//                             "TR",
//                             "DE"
//                         ]
//                     },
//                     {
//                         "type": "selector",
//                         "dimension": "accountregion",
//                         "value": "some_accountregion"
//                     }
//                 ]
//             },
//             "dimension": {
//                 "type": "default",
//                 "dimension": "nationality",
//                 "outputName": "nationality"
//             },
//             "aggregations": [
//                 {
//                     "name": "searchcount",
//                     "type": "longSum",
//                     "fieldName": "searchcount"
//                 }
//             ],
//             "metric": "searchcount",
//             "threshold": 5
//         };

//         var query3 = {
//             "queryType": "topN",
//             "dataSource": "dr_hsl",
//             "intervals": "2019-07-09T00:01Z/2019-07-10T00:01Z",
//             "granularity": "all",
//             "filter": {
//                 "type": "in",
//                 "dimension": "nationality",
//                 "values": [
//                     "RU",
//                     "TR",
//                     "DE"
//                 ]
//             },
//             "dimension": {
//                 "type": "default",
//                 "dimension": "accountregion",
//                 "outputName": "accountregion"
//             },
//             "aggregations": [
//                 {
//                     "name": "searchcount",
//                     "type": "longSum",
//                     "fieldName": "searchcount"
//                 }
//             ],
//             "metric": "searchcount",
//             "threshold": 50
//         };

//         var query4={
//             "queryType": "timeseries",
//             "dataSource": "dr_hsl",
//             "intervals": "2019-07-09T00:01Z/2019-07-10T00:01Z",
//             "granularity": "all",
//             "filter": {
//               "type": "in",
//               "dimension": "accountregion",
//               "values": [
//                 "CARIA HOLIDAYS",
//                 "API TOUR",
//                 "AGA GROUP"
//               ]
//             },
//             "aggregations": [
//               {
//                 "name": "__VALUE__",
//                 "type": "longSum",
//                 "fieldName": "searchcount"
//               }
//             ]
//           };
        
//         requesterFn(query4)
//             .then((resp) => console.log(resp.data))
//             .catch((err) => {
//                 console.log("err:", err);
//             })
//     });
// })
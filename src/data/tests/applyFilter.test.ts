import mocha from "mocha";
import { expect } from "chai";
import { GetDataSourceName, GetDataset, GetPermissions, ApplyFilters } from "../domain/queryExtensions";
import { DatasetInfo, IAppContext } from "../../core/abstractions/appcontext";
import { Role } from "../../core/abstractions/access";

describe("Apply Filter", function () {

    it("should_return_datasource_name", function () {
        const val = "sample_datasource";
        let query = { dataSource: val };
        const ds = GetDataSourceName(query);
        expect(ds).equal(val, "datasource must be exists");
    });

    it("should_return_error_if_dataset_not_defined_for_customer", function () {
        const ds = GetDataset("abcd", { erpid: "", datasets: [{ id: "1", name: "www", permissions: [] }] });
        expect(ds).to.be.equal(undefined);
    });

    it("should_get_customer_permissions_if_user_admin", function () {
        const custDsInfo: DatasetInfo = { id: "1", name: "abcd", permissions: [] };
        const perms = GetPermissions(custDsInfo, 
            { 
                userId: "", 
                name: "",
                 surname: "", 
                 customerid: "", 
                 groups: [], 
                 requestId: "1", 
                 roles: [Role.Admin],
                 route:"" });
        expect(perms).equal(custDsInfo.permissions);
    });

    it("should_get_customer_permissions_if_user_dataset_undefined", function () {
        const custDsInfo: DatasetInfo = { id: "1", name: "abcd", permissions: [] };
        const ctx: IAppContext = {
            userId: "",
            name: "",
            surname: "",
            customerid: "",
            groups: [],
            requestId: "1",
            roles: [Role.Designer],
            route:""
        };
        const perms = GetPermissions(custDsInfo, ctx);
        expect(perms).equal(custDsInfo.permissions);
    });

    it("should_get_customer_permissions_if_user_dataset_exists", function () {
        const custDsInfo: DatasetInfo = {
            id: "1",
            name: "abcd",
            permissions: [
                { hidden: true, name: "CustomerId", values: ["123456"] },
                { hidden: false, name: "AccountRegion", values: ["ABC", "DEF", "GHI"] }
            ]
        };
        const ctx: IAppContext = {
            userId: "",
            name: "",
            surname: "",
            customerid: "",
            groups: [],
            requestId: "1",
            roles: [Role.Designer],
            route:"",
            datasets: [
                {
                    id: "1",
                    name: "abcd",
                    permissions: [
                        { name: "AccountRegion", hidden: false, values: ["ABC", "GHI"] }
                    ]
                }
            ]
        };
        const perms = GetPermissions(custDsInfo, ctx);
        console.log(perms);
        expect(perms.length).equal(2);
    });

    it("should_apply_filter_with_user_dataset", function () {
        const custDsInfo: DatasetInfo = {
            id: "1",
            name: "abcd",
            permissions: [
                { hidden: true, name: "CustomerId", values: ["123456"] },
                { hidden: false, name: "AccountRegion", values: ["ABC", "DEF", "GHI"] }
            ]
        };
        const ctx: IAppContext = {
            userId: "",
            name: "",
            surname: "",
            customerid: "",
            groups: [],
            requestId: "1",
            route:"",
            roles: [Role.Designer],
            datasets: [
                {
                    id: "1",
                    name: "abcd",
                    permissions: [
                        { name: "AccountRegion", hidden: false, values: ["ABC", "GHI"] }
                    ]
                }
            ]
        };
        const perms = GetPermissions(custDsInfo, ctx);
        let query: any = { "dataSource": "abcd" };
        ApplyFilters(query, perms);
        console.log(JSON.stringify(query));
        expect(query.filter.fields.length).equal(2);
    });

})
import { SearchOnFields, SearchOnFieldsResponse } from "../../messages/query/searchOnFields";
import { HttpContextFactory, IAppContext, AppContextTypes } from "../../../core/abstractions/appcontext";
import { IsUserMemberOf } from "../../../core/extensions/role";
import { Role } from "../../../core/abstractions/access";
import { IsNullOrEmptyOrWhitespace } from "../../../core/extensions/stringextensions";
import { GetDataset, GetPermissions } from "../../domain/queryExtensions";
import { IDataRequester, RequestType } from "../../../core/abstractions/dataRequester";

export function SearchOnFieldsHandlerFactory(
    requester: IDataRequester,
    contextFactory: HttpContextFactory) {

    function getSearchQuery(req: SearchOnFields): any {
        let requestModel: any =
        {
            "queryType": "topN",
            "dataSource": req.datasource,
            "threshold": req.limit || 1000,
            "granularity": "all",
            "intervals": req.intervals,
            "dimension": req.dimension,
            "aggregations": [
                {
                    "name": "Count",
                    "type": "count"
                }
            ],
            "metric": "Count"
        };        
        if(req.filter && req.filter!=""){
            requestModel.filter= {
               "type": "search",
               "dimension":req.dimension,
               "query":{
                   "type": "insensitive_contains",
                   "value": req.filter
               }
           };
        }
        return requestModel;
    }

    async function getFieldValues(query: any, dimesion: string): Promise<string[]> {
        try {
            const resp = await requester.Query(query, RequestType.Native);
            if (resp.status != 200) return [];
            const data: any[] = resp.data;
            if (data.length == 0) return [];
            const items: any[] = data[0].result;
            return items.map(p => p[dimesion]);
        } catch (e) {
            console.log(e);
        }
        return [];
    }

    return async (req: SearchOnFields): Promise<SearchOnFieldsResponse> => {
        const ctx = contextFactory().Get<IAppContext>(AppContextTypes.Context);
        const query = getSearchQuery(req);
        let vals: string[] = [];       
        if (IsUserMemberOf(ctx.roles, [Role.Master]))
            vals = await getFieldValues(query, req.dimension);
        else {            
            const ds = GetDataset(req.datasource, ctx.customerInfo);          
            if (ds == undefined) return { items: [] };
            const permissions = GetPermissions(ds, ctx);                 
            const fieldInfo = permissions.find(p => p.name == req.dimension);           
            if (fieldInfo && fieldInfo.values && fieldInfo.values.length > 0) {               
                vals = fieldInfo.values;
            }
            else vals = await getFieldValues(query, req.dimension);
        }
        return { items: vals.filter(p=> p) };
    }
}
import { SearchBySql, SearchBySqlResponse, SearchSqlItem } from "../../messages/query/searchBySql";
import { HttpContextFactory, IAppContext, AppContextTypes } from "../../../core/abstractions/appcontext";
import { IDataRequester, RequestType } from "../../../core/abstractions/dataRequester";
import { CheckQuery, BuildSqlQuery } from "../common/sqlQueryExtension";
import { IsUserMemberOf } from "../../../core/extensions/role";
import { Role } from "../../../core/abstractions/access";

export function SearchBySqlHandlerFactory(
    requester: IDataRequester,
    contextFactory: HttpContextFactory
) {
    return async (req: SearchBySql): Promise<SearchBySqlResponse> => {        
        const ctx = contextFactory().Get<IAppContext>(AppContextTypes.Context);        
        if (!ctx.customerInfo) return { data: {} };
        let queries: SearchSqlItem[] = req.queries;
        if (!IsUserMemberOf(ctx.roles, [Role.Master]))
            queries = CheckQuery(req, ctx);        
        const strQuery = BuildSqlQuery(queries);      
        const result = await requester.Query({ query: strQuery }, RequestType.Sql);      
        if (result.status != 200) return { data: {} };
        return { data: result.data };
    }
}
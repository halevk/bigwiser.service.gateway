import { SearchSource, SearchSourceResponse } from "../../messages/query/searchSource";
import { HttpContextFactory, IAppContext, AppContextTypes } from "../../../core/abstractions/appcontext";
import { GetDataSourceName, ApplyFilters, GetDataset, GetPermissions } from "../../domain/queryExtensions";
import { IsNullOrEmptyOrWhitespace } from "../../../core/extensions/stringextensions";
import { IDataRequester, RequestType } from "../../../core/abstractions/dataRequester";


export function SearchSourceHandlerFactory(
    requester: IDataRequester,
    contextFactory: HttpContextFactory) {
    return async (req: SearchSource): Promise<SearchSourceResponse> => {
        const ctx = contextFactory().Get<IAppContext>(AppContextTypes.Context);
        const dsName = GetDataSourceName(req.query);
        if (IsNullOrEmptyOrWhitespace(dsName))
            return { err: "datasource not found", result: null };
        const ds = GetDataset(dsName, ctx.customerInfo);
        if (ds == undefined) return { err: "you can not make search on this datasource", result: null };
        const permissions = GetPermissions(ds, ctx);
        ApplyFilters(req.query, permissions);       
        const resp = await requester.Query(req.query, RequestType.Native);
        if (resp.status != 200) return { err: resp.statusText, result: null };
        return { err: "", result: resp.data };
    }
}
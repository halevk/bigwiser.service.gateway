import { GetMinMaxDate, GetMinMaxDateResponse } from "../../messages/query/getMinMaxDate";
import { IDataRequester, RequestType } from "../../../core/abstractions/dataRequester";
import { HttpContextFactory, IAppContext, AppContextTypes } from "../../../core/abstractions/appcontext";

export function GetMinMaxDateHandlerFactory(
    requester: IDataRequester,
    contextFactory: HttpContextFactory){
    return async(req:GetMinMaxDate):Promise<GetMinMaxDateResponse>=> {        
        const ctx = contextFactory().Get<IAppContext>(AppContextTypes.Context);        
        if (!ctx.customerInfo || !ctx.customerInfo.datasets.find(p=> p.name==req.datasource)) return { Min:"",Max:"" };
        const strQuery = `Select Min(__time)as Mn,Max(__time)as Mx from ${req.datasource}`;
        const result = await requester.Query({query:strQuery},RequestType.Sql);               
        if(result.status!=200) return { Min:"",Max:"" };
        const minMax = result.data[0];
        return {Min:minMax.Mn,Max:minMax.Mx};
    }
}
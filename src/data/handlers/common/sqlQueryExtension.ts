import { SearchBySql, SearchSqlItem } from "../../messages/query/searchBySql";
import { IAppContext, DatasetInfo } from "../../../core/abstractions/appcontext";
import { IsNullOrEmptyOrWhitespace } from "../../../core/extensions/stringextensions";

export function CheckQuery(req: SearchBySql, ctx: IAppContext): SearchSqlItem[] {
    let items: SearchSqlItem[] = [];
    if (!ctx.customerInfo) return items;
    for (let i = 0; i < req.queries.length; i++) {
        const query = req.queries[i];
        if (IsNullOrEmptyOrWhitespace(query.from) ||
            IsNullOrEmptyOrWhitespace(query.select)) {
            continue;
        }
        const cds = ctx.customerInfo.datasets.find(p => p.name == query.from);
        if (!cds) continue;
        if (ctx.datasets && ctx.datasets.length > 0) {
            const uds = ctx.datasets.find(p => p.name == query.from);
            processDataset(query, items, uds);
        } else processDataset(query, items, cds);
    }
    return items;
}

function processDataset(query: SearchSqlItem, items: SearchSqlItem[], ds?: DatasetInfo) {
    if (!ds || !ds.permissions || ds.permissions.length == 0) {
        items.push(query);
        return;
    }
    if (query.where && query.where.length > 0) {
        for (let q of query.where) {
            const perm = ds.permissions.find(p => p.name == q.field);
            if (perm && perm.values && perm.values.length > 0)
                perm.values = perm.values;
        }
        items.push(query);
    } else {
        query.where = [];
        for (let up of ds.permissions)
            query.where.push({ field: up.name, opr: "in", value: up.values, cond: "and" });
        items.push(query);
    }
}

function getValue(val: string | number | string[]): string {
    switch (typeof (val)) {
        case "object":
            return val.map(o => `'${o}'`).join(",");
        case "number":
            return val.toString();
        default:
            return `'${val}'`;
    }
}

export function BuildSqlQuery(items: SearchSqlItem[]): string {
    let queries: string[] = [];
    for (let q of items) {
        let qq: string = `SELECT ${q.select} `;
        qq += `FROM ${q.from} `;
        if (q.where.length > 0) {
            qq += "WHERE ";
            for (let i = 0; i < q.where.length; i++) {
                const w = q.where[i];
                qq += `${w.field}`;
                qq += (w.cond == "in") ? ` in (${getValue(w.value)}) ` : `${w.cond}${getValue(w.value)} `;
                if (i != q.where.length - 1)
                    qq += `${w.opr} `;
            }
        }
        if (!IsNullOrEmptyOrWhitespace(q.groupby))
            qq += `GROUP BY ${q.groupby} `;
        if (!IsNullOrEmptyOrWhitespace(q.having))
            qq += `HAVING ${q.having} `;
        if (!IsNullOrEmptyOrWhitespace(q.orderby))
            qq += `ORDER BY ${q.orderby} `;
        if (q.limit > -1)
            qq += `LIMIT ${q.limit}`;
        queries.push(qq);
    }

    return queries.join(" UNION ");
}
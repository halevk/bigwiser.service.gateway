import { ValidationResult } from "../../../core/abstractions/routing";

export class SearchSource {
    query: any = {};
}

export interface SearchSourceResponse {
    err: string,
    result: any
}

export function SearchSourceValidator(request: SearchSource): ValidationResult {
    const err: string[] = [];
    if (request.query == null || request.query == undefined) err.push("query is required");
    const success = err.length == 0;
    return { errors: err, success: success };
}
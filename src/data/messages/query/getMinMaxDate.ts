import { ValidationResult } from "../../../core/abstractions/routing";
import { IsNullOrEmptyOrWhitespace } from "../../../core/extensions/stringextensions";

export interface GetMinMaxDate {    
    datasource: string
}

export interface GetMinMaxDateResponse {    
    Min:string,
    Max:string
}

export function GetMinMaxDateValidator(request: GetMinMaxDate): ValidationResult {
    const err: string[] = [];
    if (IsNullOrEmptyOrWhitespace(request.datasource)) err.push("datasource is required");    
    const success = err.length == 0;
    return { errors: err, success: success };
}
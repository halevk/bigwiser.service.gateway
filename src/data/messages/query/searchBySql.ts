import { ValidationResult } from "../../../core/abstractions/routing";

export class SearchBySql {
    queries: SearchSqlItem[] = [];
}

export interface SearchSqlItem {
    select: string,
    from: string,
    where: WhereOptions[],
    groupby: string,
    having: string,
    orderby: string,
    limit: number
}

export interface WhereOptions {
    field: string,
    opr: string,
    cond: string,
    value: string[] | number | string
}

export interface SearchBySqlResponse {
    data: any
}

export function SearchBySqlValidator(request: SearchBySql): ValidationResult {
    const err: string[] = [];    
    if (request.queries.length == 0) err.push("at least a query is required");
    const success = err.length == 0;
    return { errors: err, success: success };
}
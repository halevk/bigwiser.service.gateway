import { IDashboardRepository } from "../domain/repositories/dashboard";
import { injectable, inject } from "inversify";
import { IRepository } from "../../core/abstractions/repository";
import { RepositoryType } from "../../core/abstractions/dependecyTypes";
import { Dashboard, Right } from "../domain/models/dashboard";

@injectable()
export class DashboardRepository implements IDashboardRepository {    
    
    private repository: IRepository
    collectionName: string = "dashboards";

    constructor(@inject(RepositoryType) repo: IRepository) {
        this.repository = repo;
    }

    findById(id: string): Promise<Dashboard | null> {
        return this.repository.findOneById(this.collectionName, id);
    }

    findByIdAndCustomerId(id:string,customerid:string):Promise<Dashboard | null>{
        return this.repository.findOneByQuery(this.collectionName,{"_id":id,"customerid":customerid});
    }

    save(data: Dashboard): Promise<boolean> {
        return this.repository.save(this.collectionName, data._id, data);
    }

    getByDesigner(customerid: string, designerid: string): Promise<Dashboard[]> {
        return this.repository.findMultipleByQuery<Dashboard>(this.collectionName,
            { "customerid": customerid, "designerid": designerid });
    }

    getByCustomer(customerid: string): Promise<Dashboard[]> {
        return this.repository.findMultipleByQuery(this.collectionName,
            {"customerid":customerid});
    }

    getByGroup(customerid: string, groups: string[],userid:string, right: Right): Promise<Dashboard[]> {
        return this.repository.findMultipleByQuery(this.collectionName,{
              "customerid":customerid,
              "$or":[{"permissions.groupids":{"$in":groups}},{"permissions.userids":userid}] ,
              "permissions.rights":right.valueOf()
        });
    }

    remove(id: string): Promise<boolean> {
        return this.repository.removeSingle(this.collectionName,id);
    }
}
export interface BasicDashboardInfo{
    id:string,
    title:string,
    createdBy:string,
    createdOn:string,
    updatedBy?:string,
    updatedOn?:string,
    active:boolean
}
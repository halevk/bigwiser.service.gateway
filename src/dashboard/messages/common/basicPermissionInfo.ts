export interface DetailPermissionInfo {
    groupids: string[],
    userids:string[],
    rights: string[]
}

export interface BasicRightInfo {
    right:string,
    enable:boolean
}
import { ValidationResult } from "../../../core/abstractions/routing";
import { IsNullOrEmptyOrWhitespace } from "../../../core/extensions/stringextensions";

export interface PermissionInfo {
    groupids: string[],
    userids:string[],
    rights: string[]
}

export class ChangePermission {
    id: string = "";
    permissions:PermissionInfo[]=[];
}

export interface ChangePermissionReponse {
    success: boolean,
    err: string
}

export function ChangePermissionValidator(request: ChangePermission): ValidationResult {
    const err: string[] = [];
    if (IsNullOrEmptyOrWhitespace(request.id)) err.push("id is required");   
    return { errors: err, success: err.length == 0 };
}
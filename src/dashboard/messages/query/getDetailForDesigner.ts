import { ValidationResult } from "../../../core/abstractions/routing";
import { IsNullOrEmptyOrWhitespace } from "../../../core/extensions/stringextensions";
import { DetailedDashboardInfo } from "../common/detailDashboardInfo";

export class GetDetailForDesigner {
    id:string="";
}

export interface GetDetailForDesginerResponse {
    dashboard:DetailedDashboardInfo | null   
}

export function GetDetailForDesignerValidator(request: GetDetailForDesigner): ValidationResult {
    const err: string[] = [];
    if (IsNullOrEmptyOrWhitespace(request.id)) err.push("id is required");   
    return { errors: err, success: err.length == 0 };
}
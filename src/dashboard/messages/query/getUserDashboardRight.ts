import { ValidationResult } from "../../../core/abstractions/routing";
import { IsNullOrEmptyOrWhitespace } from "../../../core/extensions/stringextensions";
import { BasicRightInfo } from "../common/basicPermissionInfo";

export class GetUserDashboardRight {
    id:string="";
    rights:string[]=[];
}

export interface GetUserDashboardRightResponse {
    rights:BasicRightInfo[],
    err:string  
}

export function GetUserDashboardRightValidator(request: GetUserDashboardRight): ValidationResult {
    const err: string[] = [];
    if (IsNullOrEmptyOrWhitespace(request.id)) err.push("dashboard id is required");
    if(request.rights.length==0) err.push("rights must be specified");   
    return { errors: err, success: err.length == 0 };
}
import { BasicDashboardInfo } from "../common/basicDashboardInfo";

export class GetDashboardList {}

export interface GetDashboardListResponse {
    dashboards:BasicDashboardInfo[]
}

import { GetUserDashboardRight, GetUserDashboardRightResponse } from "../../messages/query/getUserDashboardRight";
import { IDashboardRepository } from "../../domain/repositories/dashboard";
import { HttpContextFactory, IAppContext, AppContextTypes } from "../../../core/abstractions/appcontext";
import { Permission } from "../../domain/models/dashboard";
import { BasicRightInfo } from "../../messages/common/basicPermissionInfo";

export function GetUserDashboardRightHandler(
    repo: IDashboardRepository,
    contextFactory: HttpContextFactory
) {
    function findRights(
        rights: string[],
        groups: string[],
        userid: string,
        perms: Permission[]): BasicRightInfo[] {
        if (perms.length == 0) return mapRightInfo(rights, true);
        return rights.map(r => {
            const result = perms.map(p => (p.userids.includes(userid) || p.groupids.some(o => groups.includes(o))) && p.rights.includes(r.toLocaleLowerCase()))
                .includes(true);
            const ri: BasicRightInfo = { enable: result, right: r }
            return ri;
        });
    }
    function mapRightInfo(rights: string[], enable: boolean): BasicRightInfo[] {
        return rights.map(p => {
            const ri: BasicRightInfo = { enable: enable, right: p };
            return ri;
        });
    }
    return async (req: GetUserDashboardRight): Promise<GetUserDashboardRightResponse> => {
        const ctx = contextFactory().Get<IAppContext>(AppContextTypes.Context);
        const dashb = await repo.findByIdAndCustomerId(req.id, ctx.customerid);
        if (!dashb) return { err: "dashboard not found", rights: mapRightInfo(req.rights, false) };
        const rights = findRights(req.rights, ctx.groups, ctx.userId, dashb.permissions);
        return { err: "", rights: rights };
    }
}
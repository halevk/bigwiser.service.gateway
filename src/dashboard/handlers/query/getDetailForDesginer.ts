import { IDashboardRepository } from "../../domain/repositories/dashboard";
import { GetDetailForDesigner, GetDetailForDesginerResponse } from "../../messages/query/getDetailForDesigner";
import { DetailedDashboardInfo } from "../../messages/common/detailDashboardInfo";
import { DetailPermissionInfo } from "../../messages/common/basicPermissionInfo";

export function GetDetailForDesignerHandlerFactory(repo: IDashboardRepository) {
    return async (req: GetDetailForDesigner): Promise<GetDetailForDesginerResponse> => {
        const db = await repo.findById(req.id);
        if (db == null) return { dashboard: null };
        const resp: DetailedDashboardInfo = {
            id: db._id,
            active: db.active,
            createdBy: db.createdBy,
            createdOn: db.createdOn,
            layout: db.layout,
            permissions: db.permissions.map(p => {
                const item: DetailPermissionInfo = { 
                    groupids: p.groupids, 
                    userids:p.userids,
                    rights: p.rights };
                return item;
            }),
            title: db.title,
            updatedBy: db.updatedBy,
            updatedOn: db.updatedOn,
            widgets: db.widgets
        };
        return { dashboard: resp };
    }
}
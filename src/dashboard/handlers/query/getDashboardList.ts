import { IDashboardRepository } from "../../domain/repositories/dashboard";
import { HttpContextFactory, IAppContext, AppContextTypes } from "../../../core/abstractions/appcontext";
import { GetDashboardList, GetDashboardListResponse } from "../../messages/query/getDashboardList";
import { IsUserMemberOf } from "../../../core/extensions/role";
import { Role } from "../../../core/abstractions/access";
import { Dashboard, Right } from "../../domain/models/dashboard";
import { BasicDashboardInfo } from "../../messages/common/basicDashboardInfo";

export function GetDashboardListHandlerFactory(
    repo: IDashboardRepository,
    contextFactory: HttpContextFactory) {

    async function getData(ctx:IAppContext):Promise<Dashboard[]>{
        if (IsUserMemberOf(ctx.roles, [Role.Admin, Role.Master])) 
            return await repo.getByCustomer(ctx.customerid);
        if (IsUserMemberOf(ctx.roles, [Role.Designer])) 
            return await repo.getByDesigner(ctx.customerid, ctx.userId);
        return await repo.getByGroup(ctx.customerid, ctx.groups,ctx.userId, Right.View);
    }    

    return async (req: GetDashboardList): Promise<GetDashboardListResponse> => {
        const ctx = contextFactory().Get<IAppContext>(AppContextTypes.Context);
        const data:Dashboard[] = await getData(ctx);
        if (data.length == 0) return { dashboards: [] };
        const dbs = data.map(p => {
            const di: BasicDashboardInfo = {
                id: p._id,
                active: p.active,
                createdBy: p.createdBy,
                createdOn: p.createdOn,
                updatedBy: p.updatedBy,
                updatedOn: p.updatedOn,
                title: p.title
            };
            return di;
        });
        return { dashboards: dbs };
    }
}
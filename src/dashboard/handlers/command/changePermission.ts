import { ChangePermission, ChangePermissionReponse } from "../../messages/command/changePermission";
import { IDashboardRepository } from "../../domain/repositories/dashboard";
import { Permission } from "../../domain/models/dashboard";

export function ChangePermissionHandlerFactory(
    repo: IDashboardRepository) {
    return async (req: ChangePermission): Promise<ChangePermissionReponse> => {
        const db = await repo.findById(req.id);
        if (db == null) return { success: false, err: "dashboard not found" };
        db.permissions= req.permissions.map(p=> {
            const pm:Permission={groupids:p.groupids,userids:p.userids,rights:p.rights};
            return pm;
        });       
        const result = await repo.save(db);       
        return { 
            success: result, 
            err: !result ? "permissions could no saved":"" 
        };
    }
}
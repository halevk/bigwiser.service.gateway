import { IDashboardRepository } from "../../domain/repositories/dashboard";
import { SaveDashboard, SaveDashboardResponse } from "../../messages/command/saveDashboard";
import { HttpContextFactory, IAppContext, AppContextTypes } from "../../../core/abstractions/appcontext";
import { Dashboard } from "../../domain/models/dashboard";
import { GetUtcNowAsString } from "../../../core/extensions/date";
import { IsNullOrEmptyOrWhitespace } from "../../../core/extensions/stringextensions";
const uuidv4 = require("uuid/v4")

export function SaveDashboardHandlerFactory(
    repo: IDashboardRepository,
    contextFactory: HttpContextFactory) {
    return async (req: SaveDashboard): Promise<SaveDashboardResponse> => {
        const ctx = contextFactory().Get<IAppContext>(AppContextTypes.Context);
        const isIdMissing = IsNullOrEmptyOrWhitespace(req.id);       
        const id = isIdMissing ? uuidv4() : req.id;
        let record: Dashboard | null;
        const user = `${ctx.name} ${ctx.surname}`;
        if (!isIdMissing) {
            record = await repo.findById(id);
            if (record == null) return { success: false, msg: "dashboard does not exists" };
            record.active = req.active;
            record.layout = req.layout;
            record.title = req.title;
            record.widgets = req.widgets;
            record.updatedBy = user;
            record.updatedOn = GetUtcNowAsString();
        } else {
            record = {
                _id: id,
                active: req.active,
                layout: req.layout,
                title: req.title,
                customerid: ctx.customerid,
                permissions: [],
                widgets: req.widgets,
                designerid: ctx.userId,
                createdBy: user,
                createdOn: GetUtcNowAsString()
            };
        }
        const result = await repo.save(record);
        if (!result) return { success: false, msg: "dashboard could not saved" };
        return { success: true, msg: "saved successfully", id: id };
    }
}
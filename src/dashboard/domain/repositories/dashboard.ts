import { Dashboard, Right } from "../models/dashboard";

export interface IDashboardRepository {   
    findById(id: string): Promise<Dashboard | null>;
    save(data: Dashboard): Promise<boolean>;
    remove(id:string):Promise<boolean>;
    findByIdAndCustomerId(id:string,customerid:string):Promise<Dashboard | null>;
    getByDesigner(customerid:string,designerid:string):Promise<Dashboard[]>;
    getByCustomer(customerid:string):Promise<Dashboard[]>;
    getByGroup(customerid:string,groups:string[],userid:string,right:Right):Promise<Dashboard[]>;
}
import { IMessageRouter } from "../core/abstractions/routing";
import { Container } from "inversify";
import { IWidgetRepository } from "./domain/repositories/widget";
import { WidgetRepository } from "./repositories/widget";
import { SaveWidgetRoute, WidgetListRoute, WidgetDetailRoute, WidgetQueriesRoute, RemoveWidgetRoute } from "./messages/routes";
import { SaveWidgetHandlerFactory } from "./handlers/command/saveWidget";
import { SaveWidgetValidator } from "./messages/command/saveWidget";
import { Role } from "../core/abstractions/access";
import { IHttpContext } from "../core/abstractions/appcontext";
import { AppContextType } from "../core/abstractions/dependecyTypes";
import { WidgetListHandlerFactory } from "./handlers/query/widgetList";
import { WidgetDetailHandlerFactory } from "./handlers/query/widgetDetail";
import { WidgetDetailValidator } from "./messages/query/widgetDetail";
import { WidgetQueriesHandlerFactory } from "./handlers/query/widgetQueries";
import { WidgetQueriesValidator } from "./messages/query/widgetQueries";
import { RemoveWidgetHandlerFactory } from "./handlers/command/removeWidget";
import { RemoveWidgetValidator } from "./messages/command/removeWidget";

function getwidgetRepository(container: Container): IWidgetRepository {
    return container.get<IWidgetRepository>("IWidgetRepository");
}

export function RegisterWidgetRouters(container: Container, router: IMessageRouter): void {
    router.registerCommandHandler(SaveWidgetRoute,
        SaveWidgetHandlerFactory(
            getwidgetRepository(container),
            () => container.get<IHttpContext>(AppContextType)),
        SaveWidgetValidator, [Role.Master, Role.Admin, Role.Designer]);

    router.registerCommandHandler(RemoveWidgetRoute,
        RemoveWidgetHandlerFactory(
            getwidgetRepository(container),
            () => container.get<IHttpContext>(AppContextType)),
        RemoveWidgetValidator, [Role.Master, Role.Admin, Role.Designer]);

    router.registerCommandHandler(WidgetListRoute,
        WidgetListHandlerFactory(
            getwidgetRepository(container),
            () => container.get<IHttpContext>(AppContextType)),
        undefined, [Role.Master, Role.Admin, Role.Designer]);

    router.registerCommandHandler(WidgetDetailRoute,
        WidgetDetailHandlerFactory(
            getwidgetRepository(container)),
        WidgetDetailValidator, [Role.Master, Role.Admin, Role.Designer, Role.Viewer]);

    router.registerCommandHandler(WidgetQueriesRoute,
        WidgetQueriesHandlerFactory(
            getwidgetRepository(container)),
        WidgetQueriesValidator, [Role.Master, Role.Admin, Role.Designer, Role.Viewer]);
}

export function RegisterWidgetTypes(container: Container): void {
    container.bind<IWidgetRepository>("IWidgetRepository").to(WidgetRepository).inSingletonScope();
}
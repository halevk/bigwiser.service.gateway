import { ValidationResult } from "../../../core/abstractions/routing";
import { IsNullOrEmptyOrWhitespace } from "../../../core/extensions/stringextensions";

export class RemoveWidget {
    id: string = "";    
}

export interface RemoveWidgetResponse {
    success: boolean,
    msg: string    
}

export function RemoveWidgetValidator(request: RemoveWidget): ValidationResult {
    const err: string[] = [];
    if (IsNullOrEmptyOrWhitespace(request.id)) err.push("title is required");      
    const success = err.length == 0;
    return { errors: err, success: success };
}
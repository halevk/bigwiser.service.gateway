export interface BasicWidgetInfo {
    id:string,
    title:string,
    type:string,
    subtype:string,
    createdBy:string,
    createdOn:string
}
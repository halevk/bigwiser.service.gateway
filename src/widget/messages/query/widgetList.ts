import { BasicWidgetInfo } from "../common/basicWidgetInfo";

export class WidgetList{
    types:string[]=[];
}

export interface WidgetListResponse {
    widgets:BasicWidgetInfo[];
}
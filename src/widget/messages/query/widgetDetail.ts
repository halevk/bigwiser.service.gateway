import { DataSourceInfo } from "../common/dataSourceInfo";
import { ValidationResult } from "../../../core/abstractions/routing";
import { IsNullOrEmptyOrWhitespace } from "../../../core/extensions/stringextensions";

export class WidgetDetail {
    id:string="";
}

export interface WidgetDetailResponse {
    id:string,
    title:string,   
    createdBy:string,
    createdOn:string,
    type: string,
    subtype: string,
    sourcesToFilter: any,
    data: DataSourceInfo[],    
    options: any
}

export function WidgetDetailValidator(request: WidgetDetail): ValidationResult {
    const err: string[] = [];
    if (IsNullOrEmptyOrWhitespace(request.id)) err.push("id is required");      
    const success = err.length == 0;
    return { errors: err, success: success };

}




import { IWidgetRepository } from "../../domain/repositories/widget";
import { HttpContextFactory, IAppContext, AppContextTypes } from "../../../core/abstractions/appcontext";
import { WidgetList, WidgetListResponse } from "../../messages/query/widgetList";
import { BasicWidgetInfo } from "../../messages/common/basicWidgetInfo";

export function WidgetListHandlerFactory(
    repo: IWidgetRepository,
    contextFactory: HttpContextFactory) {
    return async (req: WidgetList): Promise<WidgetListResponse> => {
        const ctx = contextFactory().Get<IAppContext>(AppContextTypes.Context);
        const widgets = await repo.findByCustomerId(ctx.customerid);
        let winfos = widgets.map(p => {
            const itm: BasicWidgetInfo = {
                id: p._id,
                createdBy: p.createdBy,
                createdOn: p.createdOn,
                subtype: p.subtype,
                title: p.title,
                type: p.type
            };
            return itm;
        });
        if(req.types && req.types.length>0){
           req.types=req.types.map(o=> o.toLowerCase()); 
           winfos = winfos.map(p=> req.types.includes(p.type.toLowerCase())?p:null).filter(o=> o!=null) as BasicWidgetInfo[];
        }
        return { widgets: winfos };
    }
}
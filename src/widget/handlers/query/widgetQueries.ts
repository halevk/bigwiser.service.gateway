import { IWidgetRepository } from "../../domain/repositories/widget";
import { WidgetQuery } from "../../messages/common/widgetQuery";
import { WidgetQueriesResponse, WidgetQueries } from "../../messages/query/widgetQueries";
import { DataSourceInfo } from "../../messages/common/dataSourceInfo";

export function WidgetQueriesHandlerFactory(repo: IWidgetRepository) {
    return async (req: WidgetQueries): Promise<WidgetQueriesResponse> => {
        const widgets = await repo.findByIds(req.ids);
        if (widgets.length == 0) return { queries: [] };
        const queries = widgets.map(p => {
            const itm: WidgetQuery = {
                id: p._id,
                datasource: p.data.map(o => {
                    const it: DataSourceInfo = { id: o.id, query: o.query };
                    return it;
                })
            };
            return itm;
        });
        return { queries: queries };
    }
}
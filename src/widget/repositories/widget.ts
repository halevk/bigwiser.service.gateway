import { IWidgetRepository } from "../domain/repositories/widget";
import { IRepository } from "../../core/abstractions/repository";
import { RepositoryType } from "../../core/abstractions/dependecyTypes";
import { inject, injectable } from "inversify";
import { Widget } from "../domain/models/widget";
import { QueryInfo } from "../domain/models/queryInfo";

@injectable()
export class WidgetRepository implements IWidgetRepository {
    
    private repository: IRepository
    collectionName: string = "widgets";
    constructor(@inject(RepositoryType) repo: IRepository) {
        this.repository = repo;
    }

    save(data: Widget): Promise<boolean> {
        return this.repository.save(this.collectionName, data._id, data);
    }

    findById(id: string): Promise<Widget | null> {
        return this.repository.findOneById(this.collectionName, id);
    }

    findByCustomerId(customerid: string): Promise<Widget[]> {
        return this.repository.findMultipleByQuery(this.collectionName, { "customerid": customerid });
    }

    findByIds(ids: string[]): Promise<Widget[]> {
        return this.repository.findMultipleByQuery(this.collectionName, { "_id": { "$in": ids } });
    }

    getQueries(ids: string[]): Promise<QueryInfo[]> {
        return this.repository.findWithProjection(this.collectionName, 
            { "_id": { "$in": ids } }, 
            {"_id":1,"query":1});
    }

    remove(id: string): Promise<boolean> {
        return this.repository.removeSingle(this.collectionName,id);
    }

}
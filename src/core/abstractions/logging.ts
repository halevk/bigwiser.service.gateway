export enum LogType {
    Metric = "metric",
    RequestLog = "requestlog",
    Audit="audit"
}

export interface ILogger {
    write(type: LogType, item: any): Promise<boolean>;
    writeWithoutAck(type: LogType, item: any):void;
}


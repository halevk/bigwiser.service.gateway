export interface IDataOptions {
   url:string
}

export interface RequesterResult {
    data: any,
    headers: any,
    request?: any,
    status: number,
    statusText: string
}

export enum RequestType {
    Native,
    Sql
}

export interface IDataRequester {
    Query(query: any, type: RequestType): Promise<RequesterResult>;
}

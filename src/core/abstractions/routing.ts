import { Role } from "./access";

export interface IMessageBroker {
    send(route: string, data: any, comeFromMiddleware?: boolean): Promise<any>;
    publish<T>(eventName: string, data: T | any): Promise<void>;   
}

export interface ValidationResult {
    success: boolean,
    errors: string[]
}
export type EventHandler = (data: any) => Promise<void>;
export type CommandHandler = (data: any) => Promise<any>;
export type ValidatorFunc = (data: any) => ValidationResult;

export interface IHandlerInfo {
    validator?: ValidatorFunc,
    handler: CommandHandler | EventHandler[],
    roles?: Role[]
}

export interface IMessageRouter {
    registerCommandHandler(
        route: string,
        handler: CommandHandler,
        validator?: ValidatorFunc,
        roles?: Role[]): void;
    registerEventHandler(eventName: string, handler: EventHandler, validator?: ValidatorFunc): void;
    getCommandHandler(route: string): IHandlerInfo | undefined;
    getEventHandlers(eventName: string): IHandlerInfo | undefined;
}

export type CommandHandlerMiddlewareFn = (data: any, handler: CommandHandler) => Promise<any>;
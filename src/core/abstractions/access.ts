export enum Role {
    Master = "master",
    Admin = "admin",
    Viewer = "viewer",
    Designer = "designer"
}

export interface IAccessFilter {
    canAccess(roles?: Role[]): Promise<boolean>;
}
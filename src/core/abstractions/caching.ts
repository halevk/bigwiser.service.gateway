
export interface ICache {
    setSingle<T>(key: string, data: T | any,duration?:number): Promise<boolean>;
    getSingle<T>(key: string): Promise<T>;
    removeSingle(key:string):Promise<boolean>;
}
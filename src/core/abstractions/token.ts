export interface ITokenProvider {
    Sign(data:any):Promise<string>;
    Verify(token:string):Promise<any>;
}
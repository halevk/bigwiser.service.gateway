export const CacheType="ICache";
export const LoggerType="ILogger";
export const RepositoryType="IRepository";
export const TokenProviderType="ITokenProvider";
export const MessageRouterType="IMessageRouter";
export const MessageBrokerType="IMessageBroker";
export const AppContextType="IHttpContext";
export const AccessFilterType="IAccessFilter";

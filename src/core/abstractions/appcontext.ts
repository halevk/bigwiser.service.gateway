export interface IAppContext {
    ip?:string,
    requestId: string,
    userId: string,
    name: string,
    surname: string,
    customerid: string,
    datasets?: DatasetInfo[],
    customerInfo?: CustomerInfo,
    roles: string[],
    groups: string[],
    route:string
}
export interface DatasetFieldInfo {
    name: string,
    hidden: boolean,
    values: string[]
}

export interface DatasetInfo {
    id: string,
    name: string,
    permissions: DatasetFieldInfo[]
}
export interface CustomerInfo {
    erpid: string,
    datasets: DatasetInfo[]
}

export interface IHttpContext {
    Set(typ: AppContextTypes, data: any): void
    Get<T>(typ: AppContextTypes): T;
}
export type HttpContextFactory = () => IHttpContext;

export enum AppContextTypes {
    Context = "context"
}
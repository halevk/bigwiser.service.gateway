export function IsUserMemberOf(source:string[],lookupRoles:string[]):boolean{
    for(let rl of lookupRoles){
        const res = source.find(p=> p==rl);
        if(res!=undefined) return true;
    }    
    return false;
}
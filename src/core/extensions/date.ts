export function GetUtcNowAsString():string {
    return new Date().toISOString();
}
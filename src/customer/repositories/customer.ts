import { ICustomerRepository } from "../domain/abstractions";
import { Customer } from "../domain/models/customer";
import { IRepository } from "../../core/abstractions/repository";
import { RepositoryType } from "../../core/abstractions/dependecyTypes";
import { inject, injectable } from "inversify";

@injectable()
export class CustomerRepository implements ICustomerRepository {
    
    private repository: IRepository
    collectionName: string = "customers";
    constructor(@inject(RepositoryType) repo: IRepository) {
        this.repository = repo;
    }

    save(cust: Customer): Promise<boolean> {
        return this.repository.save(this.collectionName, cust._id, cust);
    }

    findById(id: string): Promise<Customer | null> {
        return this.repository.findOneById<Customer>(this.collectionName, id);
    }

    listCustomers(): Promise<Customer[]> {
        return this.repository.findMultipleByQuery<Customer>(this.collectionName,{});
    }
}
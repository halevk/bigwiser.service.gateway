import { Container } from "inversify";
import { IMessageRouter } from "../core/abstractions/routing";
import { ICustomerRepository } from "./domain/abstractions";
import { CustomerRepository } from "./repositories/customer";
import { SaveCustomerRoute } from "./messages/routes";
import { SaveCustomerHandlerFactory } from "./handlers/command/saveCustomer";
import { Role } from "../core/abstractions/access";
import { SaveCustomerValidator } from "./messages/command/saveCustomer";
import { IHttpContext } from "../core/abstractions/appcontext";
import { AppContextType } from "../core/abstractions/dependecyTypes";

function getCustomerRepository(container: Container): ICustomerRepository {
    return container.get<ICustomerRepository>("ICustomerRepository");
}


export function RegisterCustomerRouters(container: Container, router: IMessageRouter): void {
    router.registerCommandHandler(SaveCustomerRoute,
        SaveCustomerHandlerFactory(
            getCustomerRepository(container),
            () => container.get<IHttpContext>(AppContextType)),
        SaveCustomerValidator, [Role.Master]);
}

export function RegisterCustomerTypes(container: Container): void {
    container.bind<ICustomerRepository>("ICustomerRepository").to(CustomerRepository).inSingletonScope();
}
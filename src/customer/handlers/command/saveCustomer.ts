import { ICustomerRepository } from "../../domain/abstractions";
import { SaveCustomer, SaveCustomerReponse } from "../../messages/command/saveCustomer";
import { Customer } from "../../domain/models/customer";
import { IsNullOrEmptyOrWhitespace } from "../../../core/extensions/stringextensions";
import { HttpContextFactory, IAppContext, AppContextTypes, DatasetInfo, DatasetFieldInfo } from "../../../core/abstractions/appcontext";
import { GetUtcNowAsString } from "../../../core/extensions/date";
const uuidv4 = require("uuid/v4");

export function SaveCustomerHandlerFactory(
    repo: ICustomerRepository,
    contextFactory: HttpContextFactory) {
    return async (req: SaveCustomer): Promise<SaveCustomerReponse> => {
        const isNew = IsNullOrEmptyOrWhitespace(req.id);
        const id = isNew ? uuidv4() : req.id;
        const ctx = contextFactory().Get<IAppContext>(AppContextTypes.Context);
        const user = `${ctx.name} ${ctx.surname}`;        
        let customer: Customer | null = null;
        if (isNew) {
            customer = {
                _id: id,
                name: req.name,
                createdBy: user,
                createdOn: GetUtcNowAsString(),
                erpid: req.erpid,
                datasets: req.datasets
            }
        } else {
            customer = await repo.findById(id);
            if (customer == null) return { success: false, msg: "customer not found" };
            customer.name = req.name;
            customer.erpid = req.erpid;
            customer.datasets = req.datasets;
        }
        const result = await repo.save(customer);
        if (!result) return { success: false, msg: "customer could not saved" };
        return { success: true, msg: "customer saved successfully" };
    }
}
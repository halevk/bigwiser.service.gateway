import { Container, interfaces } from "inversify";
import { ICache } from "../../core/abstractions/caching";
import { RedisCache } from "./redisCache";
import { CacheType } from "../../core/abstractions/dependecyTypes";
import { ICacheOptions } from "./options";

export function RegisterCachingTypes (options:ICacheOptions,container:Container):void{
    container.bind<ICacheOptions>("ICacheOptions").toConstantValue(options);
    container.bind<ICache>(CacheType).to(RedisCache).inSingletonScope();
}
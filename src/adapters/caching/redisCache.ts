import { injectable, inject } from "inversify";
import { ICache } from "../../core/abstractions/caching";
import redis,{RedisClient} from "redis";
import { ICacheOptions } from "./options";

@injectable()
export class RedisCache implements ICache {    
    private client:RedisClient
    constructor(@inject("ICacheOptions") options:ICacheOptions){
        this.client = redis.createClient(options.port,options.host,{password:options.pwd});
    }

    setSingle<T>(key: string, data:T | any,duration?:number): Promise<boolean> {
        const jsonData = JSON.stringify(data);
        if(duration==undefined)
         return Promise.resolve(this.client.set(key,jsonData));
        else return Promise.resolve(this.client.set(key,jsonData,"EX",duration));
    }    
    
    getSingle<T>(key: string): Promise<T> {
        return new Promise((res,rej)=>{
            this.client.get(key,(err:any,reply:any)=> {
                  if(err!=null) rej(err);
                  else { const data = JSON.parse(reply); res(data);}
            });
        });        
    }

    removeSingle(key: string): Promise<boolean> {
        return Promise.resolve(this.client.del(key));
    }
}
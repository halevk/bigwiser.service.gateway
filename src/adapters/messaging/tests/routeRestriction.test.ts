import mocha from "mocha";
import { expect } from "chai";
import { IsRestrictedRoute } from "../middlewares";

describe("route restriction test",function(){
    it("should_restrict_route",function(){        
        expect(IsRestrictedRoute("/data/search",["/data/*"])).true;
        expect(IsRestrictedRoute("/data/filter/test",["/data/*"])).true;
    });

    it("should_not_restrict_route",function(){        
        expect(IsRestrictedRoute("/data/search",["/data/filter/*"])).false;
        expect(IsRestrictedRoute("/data",["/data/filter/*"])).false;
    });    
});
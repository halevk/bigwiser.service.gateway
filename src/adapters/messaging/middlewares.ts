import { CommandHandlerMiddlewareFn, CommandHandler } from "../../core/abstractions/routing";
import { HttpContextFactory, IAppContext, AppContextTypes } from "../../core/abstractions/appcontext";
import { MetricLog, RequestLog } from "./logModels";
import { GetUtcNowAsString } from "../../core/extensions/date";
import { ILogger, LogType } from "../../core/abstractions/logging";
import { ICache } from "../../core/abstractions/caching";
const uuidv4 = require("uuid/v4");

export function MetricMiddlewareFactory(
    contextFactory: HttpContextFactory,
    next: CommandHandlerMiddlewareFn,
    logger: ILogger): CommandHandlerMiddlewareFn {
    return async (data: any, handler: CommandHandler): Promise<any> => {
        const ctx = contextFactory().Get<IAppContext>(AppContextTypes.Context);
        const start = Date.now();
        const result = await next(data, handler);
        const total = Date.now() - start;
        if (ctx) {
            const log: MetricLog = {
                _id: uuidv4(),
                ip: ctx.ip,
                date: new Date(),
                requestid: ctx.requestId,
                route: ctx.route,
                user: `${ctx.name} ${ctx.surname}`,
                userid: ctx.userId,
                totalexec: total
            };
            logger.writeWithoutAck(LogType.Metric, log);
        }
        return result;
    }
}

export function IsRestrictedRoute(route: string, restrictedRoutes: string[]): boolean {
    for (let rt of restrictedRoutes) {
        const restricted = route.match(rt) != null;
        if (restricted) return true;
    }
    return false;
}

export function LogMiddlewareFactory(
    contextFactory: HttpContextFactory,
    logger: ILogger,
    cache: ICache): CommandHandlerMiddlewareFn {

    async function processLog(route: string, lg: RequestLog) {
        try {
            const routes = await cache.getSingle<string[]>("api:routes:restricted");
            if (routes != null) {
                const restricted = IsRestrictedRoute(route, routes);
                if (restricted) return;
            }
        } catch (e) { console.log(e); }

        logger.writeWithoutAck(LogType.RequestLog, lg);
    }

    return async (data: any, handler: CommandHandler): Promise<any> => {
        const ctx = contextFactory().Get<IAppContext>(AppContextTypes.Context);
        let result: any = {};
        let error: string = "";
        try {
            result = await handler(data);
        } catch (e) {
            error: JSON.stringify(e);
        }
        if (ctx) {
            const user = `${ctx.name} ${ctx.surname}`;
            let reqLog: RequestLog = {
                _id: uuidv4(),
                ip: ctx.ip,
                date: new Date(),
                requestid: ctx.requestId,
                route: ctx.route,
                user: user,
                userid: ctx.userId,
                req: JSON.stringify(data),
                resp: JSON.stringify(result),
                error: error
            };
            processLog(ctx.route, reqLog);
        }

        return result;
    }
}
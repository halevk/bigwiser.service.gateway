import { Container, interfaces } from "inversify";
import { IMessageBroker, IMessageRouter } from "../../core/abstractions/routing";
import { InMemoryBroker } from "./in-memory-broker";
import { DefaultRouter } from "./messageRouter";
import { MessageBrokerType, MessageRouterType, AccessFilterType, AppContextType, LoggerType, CacheType } from "../../core/abstractions/dependecyTypes";
import { IAccessFilter } from "../../core/abstractions/access";
import { MetricMiddlewareFactory, LogMiddlewareFactory } from "./middlewares";
import { IHttpContext } from "../../core/abstractions/appcontext";
import { ILogger } from "../../core/abstractions/logging";
import { ICache } from "../../core/abstractions/caching";


function getContext(container: Container): () => IHttpContext {
    return () => container.get<IHttpContext>(AppContextType);
}

function getLogger(container: Container): ILogger {
    return container.get<ILogger>(LoggerType);
}

function getCache(container: Container): ICache {
    return container.get<ICache>(CacheType);
}

export function RegisterMessagingTypes(container: Container): void {
    container.bind<IMessageBroker>(MessageBrokerType).toDynamicValue((ctx: interfaces.Context) => {
        return new InMemoryBroker(
            container.get<IMessageRouter>(MessageRouterType),
            container.get<IAccessFilter>(AccessFilterType),
            MetricMiddlewareFactory(
                getContext(container),
                LogMiddlewareFactory(
                    getContext(container),
                    getLogger(container),
                    getCache(container)),
                getLogger(container)));
    }).inSingletonScope();
    container.bind<IMessageRouter>(MessageRouterType).to(DefaultRouter).inSingletonScope();
}
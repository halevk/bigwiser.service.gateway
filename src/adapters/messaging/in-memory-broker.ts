
import { injectable } from "inversify";
import {
    IMessageRouter,
    CommandHandler,
    EventHandler,
    IMessageBroker,
    CommandHandlerMiddlewareFn
} from "../../core/abstractions/routing";
import { IAccessFilter } from "../../core/abstractions/access";

@injectable()
export class InMemoryBroker implements IMessageBroker {
    private router: IMessageRouter;
    private filter: IAccessFilter;
    private middlewareFn: CommandHandlerMiddlewareFn;
    constructor(
        router: IMessageRouter,
        filter: IAccessFilter,
        middlewareFn: CommandHandlerMiddlewareFn,
    ) {
        this.router = router;
        this.filter = filter;
        this.middlewareFn = middlewareFn;
    }
    async send(route: string, data: any, comeFromMiddleware?: boolean): Promise<any> {
        const handlerInfo = this.router.getCommandHandler(route);
        if (!handlerInfo || !handlerInfo.handler)
            return Promise.resolve({ error: "handler not found" });
        if (comeFromMiddleware != undefined && comeFromMiddleware == true) {
            if (handlerInfo.roles) {
                const cando = await this.filter.canAccess(handlerInfo.roles);
                if (!cando) return { err: "you are not authorized to perform this action" };
            }
        }
        if (handlerInfo.validator) {
            const validationResult = handlerInfo.validator(data);
            if (!validationResult.success)
                return Promise.resolve({ success: false, error: validationResult.errors.join(",") });
        }
        const handler = handlerInfo.handler as CommandHandler;
        return await this.middlewareFn(data, handler);        
    }   

    async publish<T>(eventName: string, data: T | any): Promise<void> {
        const handlerInfo = this.router.getEventHandlers(eventName);
        if (!handlerInfo || !handlerInfo.handler) return;
        const handlers = handlerInfo.handler as EventHandler[];
        for (let handler of handlers) {
            handler(data);
        }
    }
}


export interface MetricLog {
    _id:string,
    ip?: string,
    route: string,
    userid: string,
    user: string,
    requestid: string
    totalexec: number,
    date: Date
}

export interface RequestLog {
    _id:string,
    ip?: string,
    route: string,
    userid: string,
    user: string,
    requestid: string,
    date: Date,
    error?: string,
    req: string,
    resp: string
}
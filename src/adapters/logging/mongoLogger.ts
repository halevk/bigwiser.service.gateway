import { ILogger, LogType } from "../../core/abstractions/logging";
import { MongoClient, Db, Collection } from "mongodb";
import { inject, injectable } from "inversify";
import { ILoggerOptions } from "./options";

@injectable()
export class MongoLogger implements ILogger {
    private client: MongoClient;
    private dbName: string;

    constructor(@inject("ILoggerOptions") options:ILoggerOptions) {
        this.client = options.client;
        this.dbName = options.dbName;
    }

    async getDb(): Promise<Db> {
        return this.client.db(this.dbName);
    }

    async getCollection<T>(colName: string): Promise<Collection<T>> {
        const db = await this.getDb();
        return db.collection<T>(colName)
    }

    async write(type: LogType, item: any): Promise<boolean> {
        const col = await this.getCollection<any>(type.toString());
        const saveResult = await col.insertOne(item, { w: 1 });
        return saveResult.result.ok == 1;
    }

    async writeWithoutAck(type: LogType, item: any) {
        const col = await this.getCollection<any>(type.toString());
        col.insertOne(item, { w: 0 });        
    }

}
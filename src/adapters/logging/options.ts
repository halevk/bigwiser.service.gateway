import { MongoClient } from "mongodb";

export interface ILoggerOptions {
    dbName: string,
    client: MongoClient
}
import { Container } from "inversify/dts/container/container";
import { ILogger, LogType } from "../../core/abstractions/logging";
import { MongoLogger } from "./mongoLogger";
import { LoggerType } from "../../core/abstractions/dependecyTypes";
import { ILoggerOptions } from "./options";
import { IMessageRouter } from "../../core/abstractions/routing";

export function RegisterLoggerTypes (options:ILoggerOptions,container:Container):void{
    container.bind<ILoggerOptions>("ILoggerOptions").toConstantValue(options);
    container.bind<ILogger>(LoggerType).to(MongoLogger).inSingletonScope(); 
}

export function RegisterLoggingRouters(container: Container, router: IMessageRouter): void {    
    const iLogger = container.get<ILogger>(LoggerType);
    const commonAuditEventHandler = (logger:ILogger)=> async (data:any):Promise<void> => { 
        logger.writeWithoutAck(LogType.Audit,data); 
    }    
    router.registerEventHandler("auth:user:signin",commonAuditEventHandler(iLogger));
    router.registerEventHandler("auth:user:signout",commonAuditEventHandler(iLogger));
}


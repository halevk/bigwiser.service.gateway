import { IHttpContext, AppContextTypes } from "../../core/abstractions/appcontext";
import { injectable } from "inversify";
const httpContext = require("express-http-context");

@injectable()
export class HttpContext implements IHttpContext{
    
    Set(typ: AppContextTypes, data: any): void {
        httpContext.set(typ.toString(),data);
    }  

    Get<T>(typ: AppContextTypes): T {
        return httpContext.get(typ.toString()) as T;    
    }
}
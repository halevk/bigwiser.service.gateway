import { IHttpContext } from "../../core/abstractions/appcontext";
import { HttpContext } from "./appcontext";
import { Container } from "inversify";
import { AppContextType } from "../../core/abstractions/dependecyTypes";

export function RegisterContextTypes (container:Container):void{
    container.bind<IHttpContext>(AppContextType).to(HttpContext).inRequestScope();    
}
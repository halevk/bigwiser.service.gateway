import { IDataRequester, RequesterResult, RequestType, IDataOptions } from "../../core/abstractions/dataRequester";
import axios from "axios";
import { injectable, inject } from "inversify";

@injectable()
export class DruidRequester implements IDataRequester {
    private options: IDataOptions;

    constructor(@inject("IDataOptions") options: IDataOptions) {
        this.options = options;
    }

    async Query(query: any, type: RequestType): Promise<RequesterResult> {
        let url = this.options.url;
        if (type == RequestType.Sql) url += "sql";  
        return axios.post<any, RequesterResult>(url, query, {
            headers: {
                "Content-Type": "application/json"
            }
        });
    }

}
import { IDataOptions, IDataRequester } from "../../core/abstractions/dataRequester";
import { Container } from "inversify";
import { DruidRequester } from "./requester";

export function RegisterDruidTypes (options:IDataOptions,container:Container):void{
    container.bind<IDataOptions>("IDataOptions").toConstantValue(options);
    container.bind<IDataRequester>("IDataRequester").to(DruidRequester).inSingletonScope();    
}
import { Container, interfaces } from "inversify";
import { MongoRepository } from "./mongo";
import { IRepository } from "../../core/abstractions/repository";
import { RepositoryType } from "../../core/abstractions/dependecyTypes";
import { IRepositoryOptions } from "./options";

export function RegisterRepositoryTypes (options:IRepositoryOptions,container:Container):void{
    container.bind<IRepositoryOptions>("IRepositoryOptions").toConstantValue(options);
    container.bind<IRepository>(RepositoryType).to(MongoRepository).inSingletonScope();
}
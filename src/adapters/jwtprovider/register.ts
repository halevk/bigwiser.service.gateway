import { Container, interfaces } from "inversify";
import { ITokenProvider} from "../../core/abstractions/token";
import { JwtTokenProvider } from "./jwtTokenProvider";
import { TokenProviderType } from "../../core/abstractions/dependecyTypes";
import { ITokenOptions } from "./options";

export function RegisterTokenTypes (options:ITokenOptions,container:Container):void{
    container.bind<ITokenOptions>("ITokenOptions").toConstantValue(options);
    container.bind<ITokenProvider>(TokenProviderType).to(JwtTokenProvider).inSingletonScope();
}
import { injectable, inject } from "inversify";
import { ITokenProvider } from "../../core/abstractions/token";
import jwt from "jsonwebtoken";
import { ITokenOptions } from "./options";

@injectable()
export class JwtTokenProvider implements ITokenProvider{
    
    private options:ITokenOptions;
    constructor(@inject("ITokenOptions") options:ITokenOptions){
        this.options=options;
    }

    async Sign(data: any): Promise<string> {
        return Promise.resolve(jwt.sign(data,this.options.key,{expiresIn:this.options.expires}));
    }

    async Verify(token: string): Promise<any> {
        try{
            return jwt.verify(token,this.options.key) as any;    
        }catch(ex){
            
        }
        return Promise.resolve(null);
    }

}
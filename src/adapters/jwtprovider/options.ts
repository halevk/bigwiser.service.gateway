export interface ITokenOptions{
    key:string,
    expires:string | undefined
}
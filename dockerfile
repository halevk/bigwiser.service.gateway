FROM node:14-alpine
WORKDIR /app
COPY ./ /app
RUN npm install
RUN npm run build
CMD [ "node","publish/src/gateway/server.js" ]
EXPOSE 11000
